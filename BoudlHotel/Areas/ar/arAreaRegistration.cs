﻿using System.Web.Mvc;

namespace BoudlHotel.Areas.ar
{
    public class arAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ar";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
               name: "GalleryGroupAr",
               url: "ar/Gallery/{name}",
               defaults: new { Areas="ar", controller = "Home", action = "Gallery" },
               namespaces: new[] { "BoudlHotel.Areas.ar.Controllers" }
            );
            context.MapRoute(
               name: "GalleryGroup1Ar",
               url: "ar/Branches/{name}",
               defaults: new { Areas = "ar", controller = "Home", action = "Branches" },
               namespaces: new[] { "BoudlHotel.Areas.ar.Controllers" }
           );
            context.MapRoute(
              name: "NewsDetailsAr",
              url: "ar/News/{name}",
              defaults: new { Areas = "ar", controller = "News", action = "News" },
              namespaces: new[] { "BoudlHotel.Areas.ar.Controllers" }
          );
            context.MapRoute(
              name: "OffersDetailsAr",
              url: "ar/Offers/{name}",
              defaults: new { Areas = "ar", controller = "LatestOffers", action = "Offers" },
              namespaces: new[] { "BoudlHotel.Areas.ar.Controllers" }
          );
            context.MapRoute(
                name: "ar_default",
                url:"ar/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "BoudlHotel.Areas.ar.Controllers" }
            );
        }
    }
}