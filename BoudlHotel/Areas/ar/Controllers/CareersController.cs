﻿using BoudlHotel.Header;
using BoudlHotel.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace BoudlHotel.Areas.ar.Controllers
{
    public class CareersController : BaseController
    {
        // GET: ar/Careers
        public static string succss = string.Empty;
        public static string Error = string.Empty;
        // GET: Careers
        public async Task<ActionResult> Index()
        {
            

            using (HttpClient client = new HttpClient())
            {
                CareerApplicationDTO obj = new CareerApplicationDTO();
                CommonHeader.setHeaders(client);
                obj.HotelId = 7;
                List<JobCategoriesDTO> carList = new List<JobCategoriesDTO>();
                List<CareerApplicationDTO> careerCityList = new List<CareerApplicationDTO>();
                List<CareerApplicationDTO> careerPositionList = new List<CareerApplicationDTO>();
                //HttpResponseMessage BannerRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBannerService", Obj);
                HttpResponseMessage res = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/CareerApplService", obj);
                if (res.IsSuccessStatusCode)
                {
                    var resData = res.Content.ReadAsStringAsync().Result;
                    var CareerData = JsonConvert.DeserializeObject<CareerApplicationDTO>(resData);
                    var data = CareerData.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                carList = ds.Tables[0].AsEnumerable().Select(DataRow => new JobCategoriesDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    NameEn = DataRow.Field<string>("NameEn"),
                                    NameAr = DataRow.Field<string>("NameAr")
                                }).ToList();

                                //SelectList objModelData = new SelectList(carList, "Id", "NameEn", 0);
                                //ViewBag.JobCatList = objModelData;
                                ViewBag.JobCatList = JsonConvert.SerializeObject(carList);
                                // ViewBag.JobCatList = carList;
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.JobCatList = bnrlist;
                            }
                            if (ds.Tables[2].Rows.Count > 0)
                            {
                                careerCityList = ds.Tables[2].AsEnumerable().Select(DataRow => new CareerApplicationDTO
                                {

                                    JobCategoryId = DataRow.Field<long>("JobCategoryId"),
                                    CityId = DataRow.Field<long>("CityId"),
                                    NameEn = DataRow.Field<string>("NameEn"),
                                    NameAr = DataRow.Field<string>("NameAr")
                                }).ToList();
                                //ViewBag.Positions = careerList;PositionList
                                ViewBag.CityList = JsonConvert.SerializeObject(careerCityList);
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.Positions = bnrlist;
                            }
                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                careerPositionList = ds.Tables[1].AsEnumerable().Select(DataRow => new CareerApplicationDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    JobCategoryId = DataRow.Field<long>("JobCategoryId"),
                                    PositionEn = DataRow.Field<string>("PositionEn"),
                                    PositionAr = DataRow.Field<string>("PositionAr"),
                                    CityId = DataRow.Field<long>("CityId")

                                }).ToList();
                                //ViewBag.Positions = careerList;
                                ViewBag.PositionList = JsonConvert.SerializeObject(careerPositionList);
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.PositionList = bnrlist;
                            }
                        }
                    }
                }
                ManageDistrictsDTO Obj1 = new ManageDistrictsDTO();
                Obj1.Id = 7;
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                List<PageHeaderDTO> pageheaderlist = new List<PageHeaderDTO>();
                HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", Obj1);
                if (Ctres.IsSuccessStatusCode)
                {
                    #region Page Header Banner

                    var CtData = Ctres.Content.ReadAsStringAsync().Result;

                    var categories = JsonConvert.DeserializeObject<ManageDistrictsDTO>(CtData);
                    var data = categories.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {

                            //Page Header Banners
                            if (ds.Tables[15].Rows.Count > 0)
                            {
                                pageheaderlist = ds.Tables[15].AsEnumerable().Select(DataRow => new PageHeaderDTO
                                {
                                    HotelId = DataRow.Field<long>("HotelId"),
                                    CustomerMenuId = DataRow.Field<long>("CustomerMenuId"),
                                    MenuNameEn = DataRow.Field<string>("MenuNameEn"),
                                    MenuNameAr = DataRow.Field<string>("MenuNameAr"),
                                    ImagePath = DataRow.Field<string>("ImagePath"),
                                })
                                .Where(x => x.MenuNameEn == "CAREERS")
                                .ToList();
                                //ViewBag.PageHeader = pageheaderlist;
                                ViewBag.PageHeaderBanner = img + pageheaderlist.Select(x => x.ImagePath).FirstOrDefault();
                                ViewBag.PageHeaderClass = "";
                                //System.Web.HttpContext.Current.Session["PageHeader"] = pageheaderlist;
                            }
                            else
                            {
                                ViewBag.PageHeaderBanner = "";
                                ViewBag.PageHeaderClass = " gallery-header";
                                //System.Web.HttpContext.Current.Session["PageHeader"] = null;
                            }
                        }
                    }
                    #endregion
                }
            }

            if (Session["PageHeader"] != null)
            {
                List<PageHeaderDTO> pageHeaders = ((List<PageHeaderDTO>)Session["PageHeader"]).ToList();
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                PageHeaderDTO pageHeader = pageHeaders.Where(x => x.MenuNameEn.ToLower() == "careers").FirstOrDefault();
                if (pageHeader != null)
                {
                    ViewBag.PageHeaderBanner1 = img + pageHeader.ImagePath;
                    ViewBag.PageHeaderClass = "";
                }
                else
                {
                    ViewBag.PageHeaderBanner1 = "";
                    ViewBag.PageHeaderClass = " gallery-header";
                }
            }


            
            return View();
        
        }
        [HttpPost]
        public async Task<ActionResult> Index(CareerApplicationDTO obj, HttpPostedFileBase fileUpload)
        {
            string lang = VerifyUserSession();

            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                try
                {
                    #region Process Data
                    obj.HotelId = 7;
                    string[] validFileTypes = { "doc", "docx", "docs", "pdf" };
                    if (fileUpload != null)
                    {
                        var ext = Path.GetExtension(fileUpload.FileName).ToString().ToLower();
                        //bool isValid = false;
                        if (ext == ".pdf" || ext == ".doc" || ext == ".docx")
                        {
                            string Uid = Guid.NewGuid().ToString();
                            string directoryString = "~/Uploads/";
                            string targetPath = Server.MapPath(directoryString + Uid + ext);
                            string lastPart = fileUpload.FileName.Split('\\').Last();
                            Stream strm = fileUpload.InputStream;
                            var targetFile = Path.GetFullPath(fileUpload.FileName);
                            obj.ProfilePath = Uid + ext;
                            fileUpload.SaveAs(targetPath);
                        }
                        else
                        {
                            Error = "Please select a valid Doc/Pdf file";
                            TempData["ErrorData"] = obj;
                            return RedirectToAction("Index");
                        }
                    }
                    sendMail(obj);
                    //if()

                    HttpResponseMessage CtReq = await client.PostAsJsonAsync("api/ContactAPI/AddCareerRequest", obj);
                    if (CtReq.IsSuccessStatusCode)
                    {
                        var responseData = CtReq.Content.ReadAsStringAsync().Result;
                        var res = JsonConvert.DeserializeObject<CareerApplicationDTO>(responseData);
                        string msg = res.message;
                        if (msg == "1")
                        {
                            succss = "Career Request Saved Successfully";
                            ViewData["Success"] = succss.ToString();
                        }


                        else if (msg == "4")
                        {
                            TempData["ErrorData"] = obj;
                            Error = "Failed to Save Career Request";
                            ViewData["Error"] = Error.ToString();
                        }
                    }
                    else
                    {
                        TempData["ErrorData"] = obj;
                        Error = "Failed to Insert/Update Career Request";
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return RedirectToAction("Index", "Careers");
                }
            }
            //Prepare default data
            using (HttpClient client = new HttpClient())
            {
                #region prepare Default Data
                CareerApplicationDTO obj1 = new CareerApplicationDTO();
                CommonHeader.setHeaders(client);
                obj1.HotelId = 7;
                List<JobCategoriesDTO> carList = new List<JobCategoriesDTO>();
                List<CareerApplicationDTO> careerCityList = new List<CareerApplicationDTO>();
                List<CareerApplicationDTO> careerPositionList = new List<CareerApplicationDTO>();
                //HttpResponseMessage BannerRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBannerService", Obj);
                HttpResponseMessage res = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/CareerApplService", obj1);
                if (res.IsSuccessStatusCode)
                {
                    var resData = res.Content.ReadAsStringAsync().Result;
                    var CareerData = JsonConvert.DeserializeObject<CareerApplicationDTO>(resData);
                    var data = CareerData.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                carList = ds.Tables[0].AsEnumerable().Select(DataRow => new JobCategoriesDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    NameEn = DataRow.Field<string>("NameEn"),
                                    NameAr = DataRow.Field<string>("NameAr")
                                }).ToList();

                                //SelectList objModelData = new SelectList(carList, "Id", "NameEn", 0);
                                //ViewBag.JobCatList = objModelData;
                                ViewBag.JobCatList = JsonConvert.SerializeObject(carList);
                                // ViewBag.JobCatList = carList;
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.JobCatList = bnrlist;
                            }
                            if (ds.Tables[2].Rows.Count > 0)
                            {
                                careerCityList = ds.Tables[2].AsEnumerable().Select(DataRow => new CareerApplicationDTO
                                {

                                    JobCategoryId = DataRow.Field<long>("JobCategoryId"),
                                    CityId = DataRow.Field<long>("CityId"),
                                    NameEn = DataRow.Field<string>("NameEn"),
                                    NameAr = DataRow.Field<string>("NameAr")
                                }).ToList();
                                //ViewBag.Positions = careerList;PositionList
                                ViewBag.CityList = JsonConvert.SerializeObject(careerCityList);
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.Positions = bnrlist;
                            }
                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                careerPositionList = ds.Tables[1].AsEnumerable().Select(DataRow => new CareerApplicationDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    JobCategoryId = DataRow.Field<long>("JobCategoryId"),
                                    PositionEn = DataRow.Field<string>("PositionEn"),
                                    PositionAr = DataRow.Field<string>("PositionAr"),
                                    CityId = DataRow.Field<long>("CityId")

                                }).ToList();
                                //ViewBag.Positions = careerList;
                                ViewBag.PositionList = JsonConvert.SerializeObject(careerPositionList);
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.PositionList = bnrlist;
                            }
                        }
                    }
                }
                #endregion
            }
            if (Session["PageHeader"] != null)
            {
                List<PageHeaderDTO> pageHeaders = ((List<PageHeaderDTO>)Session["PageHeader"]).ToList();
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                PageHeaderDTO pageHeader = pageHeaders.Where(x => x.MenuNameEn.ToLower() == "careers").FirstOrDefault();
                if (pageHeader != null)
                {
                    ViewBag.PageHeaderBanner = img + pageHeader.ImagePath;
                    ViewBag.PageHeaderClass = "";
                }
                else
                {
                    ViewBag.PageHeaderBanner = "";
                    ViewBag.PageHeaderClass = " gallery-header";
                }
            }


            if (lang == "En")
            {
                return View();
            }
            else
            {
                return View("Index_Ar");
            }
            //return View();
        }

        private void sendMail(CareerApplicationDTO _Contact)
        {
            string mailTo = _Contact.Email;
            string mailfrom = System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString();

            string mailBCC = string.Empty;
            

            
            string logoPath = System.Configuration.ConfigurationManager.AppSettings["mailLogo"].ToString();

            string URL = System.Configuration.ConfigurationManager.AppSettings["URL"].ToString();


            string Subject = "Career Application Request";

            string strHTML = @"<html><body><div align='center' style='width: 570px; font-family: Verdana, Geneva, sans-serif;
                                border: 1px solid #9C0606   display: block;'>
                                <table style='width: 570px; width: 51px; height: 28px; margin: 0px; border: 1px solid #9C0606'>
                                <tr><td><table style='margin-left: 0px; width: 570px;'><tr><td align='left'><a href='" + URL + @"' target='_blank'>
                                <img src='" + logoPath + @"' alt='Globus Logistics' width='240px' height='120px' style='border: 0px' />
                                </a></td></tr></table></td></tr><tr><td><hr /></td></tr><tr><td>
                                <table border='0px' cellpadding='0px' cellspacing='0px' style='width: 570px;'>
                                <tr style='height: 10px;'><td></td></tr><tr><td align='left'>
                                <font face='Verdana' style='font-size: 11px'>Dear <b>Admin</b>,</font>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'>
                                <font face='Verdana' style='font-size: 11px'>The following customer submitted a query from the website</font>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'><p align='justify'>
                                <font face='Verdana' style='font-size: 11px'><strong>Name:</strong> " + _Contact.FullName + @"<br />
                                <strong>Email:</strong> " + _Contact.Email + @" <br /><strong>Mobile:</strong> " + _Contact.Phone + @"<br />
                                <strong>Comment:</strong> <br />" /*+ _Contact.Message +*/+ @" </font></p></td></tr><tr><td style='height: 8px;'>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'>
                                <p><font face='Verdana' style='font-size: 11px'>
                                As this is an automated response, please do not reply to this email.</font>
                                </p></td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='right'>
                                <font face='Verdana' style='font-size: 11px'>Info Team</font>
                                </td></tr><tr style='height: 10px;'><td></td></tr></table></td></tr></table>
                                <br /></div></body></html>";
            string ClientHTML = @"<html><body><div align='center' style='width: 570px; font-family: Verdana, Geneva, sans-serif;
                                border: 1px solid #9C0606   display: block;'>
                                <table style='width: 570px; width: 51px; height: 28px; margin: 0px; border: 1px solid #9C0606'>
                                <tr><td><table style='margin-left: 0px; width: 570px;'><tr><td align='left'><a href='" + URL + @"' target='_blank'>
                                <img src='" + logoPath + @"' alt='Globus Logistics' width='240px' height='120px' style='border: 0px' />
                                </a></td></tr></table></td></tr><tr><td><hr /></td></tr><tr><td>
                                <table border='0px' cellpadding='0px' cellspacing='0px' style='width: 570px;'>
                                <tr style='height: 10px;'><td></td></tr><tr><td align='left'>
                                <font face='Verdana' style='font-size: 11px'>Dear <strong>" + _Contact.FullName + @"</strong>,</font>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'>
                                <font face='Verdana' style='font-size: 11px'>Your Request Query has been placed successfully, Our Executive will contact you Shortly.</font>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'><p align='justify'>
                                <font face='Verdana' style='font-size: 11px'><strong>Name:</strong> " + _Contact.FullName + @"<br />
                                <strong>Email:</strong> " + _Contact.Email + @" <br /><strong>Mobile:</strong> " + _Contact.Phone + @"<br />
                                <strong>Comment:</strong> <br />" /*+ _Contact.Message +*/+ @" </font></p></td></tr><tr><td style='height: 8px;'>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'>
                                <p><font face='Verdana' style='font-size: 11px'>
                                As this is an automated response, please do not reply to this email.</font>
                                </p></td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='right'>
                                <font face='Verdana' style='font-size: 11px'>Info Team</font>
                                </td></tr><tr style='height: 10px;'><td></td></tr></table></td></tr></table>
                                <br /></div></body></html>";
            Common.sendEmail(mailfrom, mailTo, strHTML, ClientHTML, Subject, mailBCC);
        }
    }
}