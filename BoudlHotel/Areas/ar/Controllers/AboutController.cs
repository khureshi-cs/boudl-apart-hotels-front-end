﻿using BoudlHotel.Header;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace BoudlHotel.Areas.ar.Controllers
{
    public class AboutController : BaseController
    {
        // GET: About
        public async Task<ActionResult> Index()
        {
            //string lang = VerifyUserSession();

            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                ManageAboutUsDTO Obj = new ManageAboutUsDTO();
                Obj.HotelId = 7;
                List<ManageAboutUsDTO> abtUsList = new List<ManageAboutUsDTO>();
                List<PageHeaderDTO> pageheaderlist = new List<PageHeaderDTO>();
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                ViewBag.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                HttpResponseMessage AbtResp = await client.PostAsJsonAsync("api/AboutUsAPI/NewGetABoutUsByHotel", Obj);
                //HttpResponseMessage BannerRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBannerService", Obj);
                if (AbtResp.IsSuccessStatusCode)
                {
                    var AbtData = AbtResp.Content.ReadAsStringAsync().Result;
                    var abtRes = JsonConvert.DeserializeObject<ManageAboutUsDTO>(AbtData);
                    var data = abtRes.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                abtUsList = ds.Tables[0].AsEnumerable().Select(DataRow => new ManageAboutUsDTO
                                {
                                    HotelId = DataRow.Field<long>("HotelId"),
                                    hotelname = DataRow.Field<string>("NameEn"),
                                    ContentEn = DataRow.Field<string>("ContentEn"),
                                    ContentAr = DataRow.Field<string>("ContentAr"),
                                    ImageEn = img + "/" + DataRow.Field<string>("ImageEn"),
                                    ImageAr = img + "/" + DataRow.Field<string>("ImageAr")
                                }).ToList();
                                ViewBag.AboutusList = abtUsList;
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.AboutusList = bnrlist;
                            }
                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                pageheaderlist = ds.Tables[1].AsEnumerable().Select(DataRow => new PageHeaderDTO
                                {
                                    HotelId = DataRow.Field<long>("HotelId"),
                                    CustomerMenuId = DataRow.Field<long>("CustomerMenuId"),
                                    MenuNameEn = DataRow.Field<string>("MenuNameEn"),
                                    MenuNameAr = DataRow.Field<string>("MenuNameAr"),
                                    ImagePath = img + "/" + DataRow.Field<string>("ImagePath"),
                                }).Where(x => x.MenuNameEn == "ABOUTUS").ToList();
                                ViewBag.PageHeader = pageheaderlist;
                                ViewBag.PageHeaderBanner = pageheaderlist.Select(x => x.ImagePath).FirstOrDefault();
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.PageHeader = bnrlist;
                                ViewBag.PageHeaderBanner = "gallery-header";
                            }
                        }
                    }
                }

            }

            if (Session["PageHeader"] != null)
            {
                List<PageHeaderDTO> pageHeaders = ((List<PageHeaderDTO>)Session["PageHeader"]).ToList();
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                PageHeaderDTO pageHeader = pageHeaders.Where(x => x.MenuNameEn.ToLower() == "aboutus").FirstOrDefault();
                if (pageHeader != null)
                {
                    ViewBag.PageHeaderBanner1 = img + pageHeader.ImagePath;
                    ViewBag.PageHeaderClass = "";
                }
                else
                {
                    ViewBag.PageHeaderBanner1 = "";
                    ViewBag.PageHeaderClass = "";
                }
            }

            return View();
        }

    }
}