﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using BoudlHotel.Header;
using Newtonsoft.Json;

namespace BoudlHotel.Areas.ar.Controllers
{
    [RoutePrefix("Gallery")]
    public class HomeController : BaseController
    {
        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                ManageDistrictsDTO Obj = new ManageDistrictsDTO();
                Obj.Id = 7;
                List<ManageCitiesDTO> citylst = new List<ManageCitiesDTO>();
                List<ManageBranchesDTO> branchList = new List<ManageBranchesDTO>();
                List<ManageWeeklyOffersDTO> weeklyOffers = new List<ManageWeeklyOffersDTO>();
                List<HomePageAboutSectionDTO> aboutList = new List<HomePageAboutSectionDTO>();
                List<ManageCountriesDTO> countryList = new List<ManageCountriesDTO>();
                List<HotelBannersDTO> bannersList = new List<HotelBannersDTO>();
                List<CustomMenuDTO> HomeMenuList = new List<CustomMenuDTO>();
                List<SpecialPackage> specialPackages = new List<SpecialPackage>();
                List<HighlightsDTO> highlights = new List<HighlightsDTO>();

                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                ViewBag.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                //HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService/?Id=7", Id );
                HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", Obj);
                HttpResponseMessage BannerRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBannerService", Obj);

                if (BannerRes.IsSuccessStatusCode)
                {
                    #region HomePageData
                    var bnrData = BannerRes.Content.ReadAsStringAsync().Result;
                    var banners = JsonConvert.DeserializeObject<ManageDistrictsDTO>(bnrData);
                    var data = banners.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                bannersList = (from row in ds.Tables[0].AsEnumerable()
                                                   where row.Field<string>("Language") != "2"
                                                   select new HotelBannersDTO
                                                   {
                                                       Id = Convert.ToInt64(row.Field<long>("Id")),
                                                       SourcePath = row.Field<string>("SourcePath"),
                                                       MediaTypeIdId = row.Field<Int64>("MediaTypeId")
                                                   }
                                                ).ToList();
                                

                                bannersList = bannersList.OrderByDescending(i => i.MediaTypeIdId).ToList();
                                long mediaTypeId = 0;

                                if (bannersList.Count > 0)
                                {
                                    mediaTypeId = bannersList.First().MediaTypeIdId;
                                    switch (bannersList.First().MediaTypeIdId)
                                    {

                                        case 1:
                                            ViewBag.MediaType = "1";
                                            break;
                                        case 2:
                                            ViewBag.MediaType = "2";
                                            break;
                                        case 3:
                                            ViewBag.MediaType = "3";
                                            break;
                                    }
                                    bannersList = bannersList.Where(i => i.MediaTypeIdId == mediaTypeId).ToList();
                                }

                                ViewBag.BannersList = bannersList;

                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.BannersList = bnrlist;
                            }

                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                specialPackages = ds.Tables[1].AsEnumerable()
                                                    .Select(dr => new SpecialPackage
                                                    {
                                                        Id = dr.Field<Int64>("Id"),
                                                        HotelId = dr.Field<Int64>("HotelId"),
                                                        BranchId = dr.Field<Int64>("BranchId"),
                                                        bookingURL = dr.Field<String>("bookingURL"),
                                                        NameEn = dr.Field<String>("NameEn"),
                                                        NameAr = dr.Field<String>("NameAr"),
                                                        DescriptionEn = dr.Field<String>("DescriptionEn"),
                                                        DescriptionAr = dr.Field<String>("DescriptionAr"),
                                                        ImageEn = img + dr.Field<String>("ImageEn"),
                                                        ImageAr = img + dr.Field<String>("ImageAr"),
                                                        StartDate = dr.Field<DateTime>("StartDate"),
                                                        EndDate = dr.Field<DateTime>("EndDate"),
                                                        SortIndex = dr.Field<Int32>("SortIndex"),
                                                        BookingRedirection = dr.Field<bool>("BookingRedirection"),
                                                        AmenitiesEn = dr.Field<String>("AmenitiesEn"),
                                                        AmenitiesAr = dr.Field<String>("AmenitiesAr"),
                                                        OfferType = dr.Field<Int32>("OfferType"),
                                                    }).Where(i => i.OfferType == 1).ToList();

                                ViewBag.SpecialPackages = specialPackages;
                            }
                            else
                            {
                                ViewBag.SpecialaPackages = null;

                            }
                            if (ds.Tables[2].Rows.Count > 0)
                            {
                                highlights = ds.Tables[2].AsEnumerable()
                                                    .Select(dr => new HighlightsDTO
                                                    {
                                                        Id = dr.Field<Int64>("Id"),
                                                        HotelId = dr.Field<Int64>("HotelId"),
                                                        ImageEn = img + dr.Field<String>("ImageEn"),
                                                        ImageAr = img + dr.Field<String>("ImageAr"),
                                                        CaptionEn = dr.Field<String>("CaptionEn"),
                                                        CaptionAr = dr.Field<String>("CaptionAr"),
                                                        SortIndex = dr.Field<Int32>("SortIndex")
                                                    }).ToList();
                                ViewBag.Highlights = highlights;

                            }
                            else
                            {
                                ViewBag.Highlights = null;
                            }
                        }
                    }
                    #endregion 
                }
                if (Ctres.IsSuccessStatusCode)
                {
                    #region MenuPreparation

                    var CtData = Ctres.Content.ReadAsStringAsync().Result;
                    var categories = JsonConvert.DeserializeObject<ManageDistrictsDTO>(CtData);
                    var data = categories.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            //Hotel Weekly Top Offers
                            if (ds.Tables[2].Rows.Count > 0)
                            {
                                weeklyOffers = ds.Tables[2].AsEnumerable().Select(DataRow => new ManageWeeklyOffersDTO
                                {
                                    HotelId = DataRow.Field<long>("HotelId"),
                                    HotelName = DataRow.Field<string>("HotelName"),
                                    OfferOneImageEn = img + "/" + DataRow.Field<string>("OfferOneImageEn"),
                                    OfferOneImageAr = img + "/" + DataRow.Field<string>("OfferOneImageAr"),
                                    OfferTwoImageEn = img + "/" + DataRow.Field<string>("OfferTwoImageEn"),
                                    OfferTwoImageAr = img + "/" + DataRow.Field<string>("OfferTwoImageAr"),
                                }).ToList();
                                //ViewBag.BrnList = new SelectList(branchList, "BookingUrl", "NameEn", 0);
                                ViewBag.BrnList1 = new SelectList(weeklyOffers);
                                ViewBag.WeeklyOffersList = weeklyOffers;
                            }
                            else
                            {
                                SelectList Branchlist = new SelectList("", 0);
                                ViewBag.WeeklyOffersList = Branchlist;
                            }
                            //Hotel Home page Aboutus Section
                            if (ds.Tables[5].Rows.Count > 0)
                            {
                                aboutList = ds.Tables[5].AsEnumerable().Select(DataRow => new HomePageAboutSectionDTO
                                {
                                    HotelId = DataRow.Field<long>("HotelId"),
                                    DescriptionEn = DataRow.Field<string>("DescriptionEn"),
                                    DescriptionAr = DataRow.Field<string>("DescriptionAr"),
                                    ImageEn = img + "/" + DataRow.Field<string>("ImageEn"),
                                    ImageAr = img + "/" + DataRow.Field<string>("ImageAr"),
                                }).ToList();
                                ViewBag.HomeAboutList = aboutList;
                            }
                            else
                            {
                                SelectList aboutList1 = new SelectList("", 0);
                                //ViewBag.HomeAboutList = aboutList1;
                                ViewBag.HomeAboutList = null;
                            }
                            //Countries List
                            //if (ds.Tables[7].Rows.Count > 0)
                            //{
                            //    countryList = ds.Tables[7].AsEnumerable().Select(DataRow => new ManageCountriesDTO
                            //    {
                            //        Id = DataRow.Field<long>("Id"),
                            //        NameEn = DataRow.Field<string>("NameEn"),
                            //        NameAr = DataRow.Field<string>("NameAr"),
                            //    }).ToList();
                            //    ViewBag.CountriesList = countryList;
                            //    Session["CountriesList"] = countryList;
                            //}
                            //else
                            //{
                            //    SelectList CountryList1 = new SelectList("", 0);
                            //    ViewBag.CountriesList = CountryList1;
                            //}

                            //Home Main menu List
                            //if (ds.Tables[8].Rows.Count > 0)
                            //{
                            //    HomeMenuList = ds.Tables[8].AsEnumerable().Select(DataRow => new CustomMenuDTO
                            //    {
                            //        Id = DataRow.Field<Int64>("Id"),
                            //        NameEn = DataRow.Field<string>("NameEn"),
                            //        NameAr = DataRow.Field<string>("NameAr"),
                            //        DynamicBranches=DataRow.Field<bool>("DynamicBranches"),
                            //        URL=DataRow.Field<string>("URL"),
                            //        SubUrl=DataRow.Field<string>("SubUrl"),
                            //        SortIndex=DataRow.Field<int>("SortIndex")
                            //    }).ToList();
                            //    ViewBag.HomeMenuList = HomeMenuList;
                            //    Session["HomeMenuList"] = HomeMenuList;
                            //}
                            //else
                            //{
                            //    SelectList HomeMenuList1 = new SelectList("", 0);
                            //    ViewBag.HomeMenuList = HomeMenuList1;
                            //}

                        }
                    }
                    #endregion
                }

            }
            return View();
        }
        
        public async Task<ActionResult> Gallery(string name)
        {
            
            using (HttpClient client = new HttpClient())
            {

                CommonHeader.setHeaders(client);
                ManageDistrictsDTO Obj = new ManageDistrictsDTO();
                Obj.Id = 7;
                //Obj.NameEn = "Palastine";
                Obj.NameEn = name;
                List<ManageGalleryDTO> galleryList = new List<ManageGalleryDTO>();
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                ViewBag.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", Obj);
                //HttpResponseMessage BannerRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBannerService", Obj);
                //cou = img + "/" + DataRow.Field<string>("SourcePath")
                if (Ctres.IsSuccessStatusCode)
                {
                    var bnrData = Ctres.Content.ReadAsStringAsync().Result;
                    var banners = JsonConvert.DeserializeObject<ManageGalleryDTO>(bnrData);
                    var data = banners.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[9].Rows.Count > 0)
                            {
                                galleryList = ds.Tables[9].AsEnumerable().Select(DataRow => new ManageGalleryDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    HotelId = DataRow.Field<long>("HotelId"),
                                    BranchId = DataRow.Field<long>("BranchId"),
                                    BranchName = DataRow.Field<string>("BranchNameEn"),
                                    ImagePath = DataRow.Field<string>("ImagePath"),
                                    BranchNameEn = DataRow.Field<string>("BranchNameEn"),
                                    BranchNameAr = DataRow.Field<string>("BranchNameAr"),
                                    MediaTypeId = DataRow.Field<long>("MediaTypeId"),
                                    ThumbnailImage = DataRow.Field<String>("ThumbnailImage"),

                                }).ToList();
                                ViewBag.BranchName = ds.Tables[9].AsEnumerable().Select(DataRow => new ManageGalleryDTO
                                {
                                    BranchName = DataRow.Field<string>("BranchNameEn"),
                                }).FirstOrDefault().BranchName;
                                ViewBag.Galleries = galleryList;
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.Galleries = bnrlist;
                            }
                        }
                    }
                }

            }

            if (Session["PageHeader"] != null)
            {
                List<PageHeaderDTO> pageHeaders = ((List<PageHeaderDTO>)Session["PageHeader"]).ToList();
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                PageHeaderDTO pageHeader = pageHeaders.Where(x => x.MenuNameEn.ToLower() == "gallery").FirstOrDefault();
                if (pageHeader != null)
                {
                    ViewBag.PageHeaderBanner = img + pageHeader.ImagePath;
                    ViewBag.PageHeaderClass = "";
                }
                else
                {
                    ViewBag.PageHeaderBanner = "";
                    ViewBag.PageHeaderClass = " gallery-header";
                }
            }

            return View();
            
        }
        public async Task<ActionResult> Branches(string name)
        {
            
            using (HttpClient client = new HttpClient())
            {

                CommonHeader.setHeaders(client);
                ManageDistrictsDTO Obj = new ManageDistrictsDTO();
                Obj.Id = 7;
                //Obj.NameEn = "Palastine";
                Obj.NameEn = name;
                List<BranchBannersDTO> BranchBnrList = new List<BranchBannersDTO>();
                List<ManageBranchesDTO> BranchDetails = new List<ManageBranchesDTO>();
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                ViewBag.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", Obj);
                //HttpResponseMessage BannerRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBannerService", Obj);
                //cou = img + "/" + DataRow.Field<string>("SourcePath")
                if (Ctres.IsSuccessStatusCode)
                {
                    var bnrData = Ctres.Content.ReadAsStringAsync().Result;
                    var banners = JsonConvert.DeserializeObject<BranchBannersDTO>(bnrData);
                    var data = banners.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[10].Rows.Count > 0)
                            {
                                BranchBnrList = (from row in ds.Tables[10].AsEnumerable()
                                                     where row.Field<string>("Language") == "1"
                                                     select new BranchBannersDTO
                                                     {
                                                         Id = Convert.ToInt64(row.Field<long>("Id")),
                                                         SourcePath = row.Field<string>("SourcePath"),
                                                         MediaTypId = row.Field<Int64>("MediaTypeId")
                                                     }
                                                ).ToList();
                                
                                BranchBnrList = BranchBnrList.OrderByDescending(i => i.MediaTypId).ToList();
                                long mediaTypeId =0;

                                if (BranchBnrList.Count > 0)
                                {
                                    mediaTypeId = BranchBnrList.FirstOrDefault().MediaTypId;
                                    switch (BranchBnrList.First().MediaTypId)
                                    {
                                        case 1:
                                            ViewBag.MediaType = "1";
                                            break;
                                        case 2:
                                            ViewBag.MediaType = "2";
                                            break;
                                        case 3:
                                            ViewBag.MediaType = "3";
                                            break;
                                    }
                                }

                                BranchBnrList = BranchBnrList.Where(i => i.MediaTypId == mediaTypeId).ToList();


                                ViewBag.BranchBanners = BranchBnrList;

                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.BranchBanners = bnrlist;
                            }
                            if (ds.Tables[11].Rows.Count > 0)
                            {
                                //var user = JsonConvert.DeserializeObject<ManageBranchesDTO>(responseData);
                                BranchDetails = ds.Tables[11].AsEnumerable().Select(DataRow => new ManageBranchesDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    HotelId = DataRow.Field<long>("HotelId"),
                                    NameEn = DataRow.Field<string>("NameEn"),
                                    NameAr = DataRow.Field<string>("NameAr"),
                                    AboutEn = DataRow.Field<string>("AboutEn"),
                                    AboutAr = DataRow.Field<string>("AboutAr"),
                                    BookingUrl = DataRow.Field<string>("BookingUrl"),
                                    Latitude = DataRow.Field<string>("Latitude"),
                                    Longitude = DataRow.Field<string>("Longitude"),
                                    AmenityIds = DataRow.Field<string>("AmenityIds"),
                                    FacilityBgImagePath = DataRow.Field<string>("Imagepath"),
                                }).ToList();
                                var Amdetails = ds.Tables[11].AsEnumerable().Select(DataRow => new ManageBranchesDTO
                                {
                                    //Id = DataRow.Field<long>("Id"),
                                    //HotelId = DataRow.Field<long>("HotelId"),
                                    //NameEn = DataRow.Field<string>("NameEn"),
                                    //NameAr = DataRow.Field<string>("NameAr"),
                                    //AboutEn = DataRow.Field<string>("AboutEn"),
                                    //AboutAr = DataRow.Field<string>("AboutAr"),
                                    //BookingUrl = DataRow.Field<string>("BookingUrl"),
                                    Latitude = DataRow.Field<string>("Latitude"),
                                    Longitude = DataRow.Field<string>("Longitude"),
                                    AmenityIds = DataRow.Field<string>("AmenityIds")
                                }).FirstOrDefault();
                                ManageHotelAmenitiesDTO aObj = new ManageHotelAmenitiesDTO();
                                HttpResponseMessage AmenitiesRes = await client.PostAsJsonAsync("api/HotelAmenitiesAPI/NewGetHotelAmenity", aObj);
                                if (AmenitiesRes.IsSuccessStatusCode)
                                {
                                    var AmenitiesData = AmenitiesRes.Content.ReadAsStringAsync().Result;
                                    var AmenitiesList = JsonConvert.DeserializeObject<List<ManageHotelAmenitiesDTO>>(AmenitiesData);
                                    List<ManageHotelAmenitiesDTO> AmnList = AmenitiesList;
                                    //SelectList objModelData = new SelectList(AmnList, "Id", "NameEn", 0);
                                    DataTable dt = new DataTable();
                                    dt.Columns.Add("Id");
                                    dt.Columns.Add("NameEn");
                                    dt.Columns.Add("NameAr");
                                    DataRow dr = null;
                                    string Amntids = string.Empty;
                                    Amntids = String.Join(",", Amdetails.AmenityIds.Select(p => p.ToString()).ToArray());
                                    string[] values = Amntids.Split(',');
                                    //string[] values = amen.ToString().Split(',');
                                    //string[] val = amen.ToString().Split(',');
                                    for (int i = 0; i < values.Length; i++)
                                    {
                                        if (values[i] != "")
                                        {
                                            dr = dt.NewRow();
                                            foreach (var item in AmnList)
                                            {
                                                if (item.Id.ToString() == values[i])
                                                {
                                                    dr["Id"] = item.Id;
                                                    dr["NameEn"] = item.NameEn;
                                                    dr["NameAr"] = item.NameAr;
                                                    dt.Rows.Add(dr);
                                                }
                                            }
                                        }
                                        //values[i] = values[i].Trim();
                                    }
                                    List<ManageHotelAmenitiesDTO> AmentList = new List<ManageHotelAmenitiesDTO>();
                                    AmentList = (from DataRow dr1 in dt.Rows
                                                 select new ManageHotelAmenitiesDTO()
                                                 {
                                                     Id = Convert.ToInt64(dr1["Id"]),
                                                     NameEn = dr1["NameEn"].ToString(),

                                                 }).ToList();
                                    List<DataRow> rows = dt.Rows.Cast<DataRow>().ToList();
                                    SelectList objModelData = new SelectList(rows, "Id", "NameEn", 0);
                                    ViewBag.AmenitiesList = AmentList;
                                }



                                //BranchDetails = ds.Tables[11].AsEnumerable().Select(DataRow => new ManageBranchesDTO
                                //{
                                //    Id = DataRow.Field<long>("Id"),
                                //    HotelId = DataRow.Field<long>("HotelId"),
                                //    NameEn = DataRow.Field<string>("NameEn"),
                                //    NameAr = DataRow.Field<string>("NameAr"),
                                //    AboutEn=DataRow.Field<string>("AboutEn"),
                                //    AboutAr=DataRow.Field<string>("AboutAr"),
                                //    BookingUrl=DataRow.Field<string>("BookingUrl"),
                                //    Latitude=DataRow.Field<string>("Latitude"),
                                //    Longitude=DataRow.Field<string>("Longitude"),

                                //}).ToList();
                                ViewBag.BranchDetails = BranchDetails;
                                ViewData["latitude"] = Amdetails.Latitude;
                                ViewData["longitude"] = Amdetails.Longitude;
                            }
                            else
                            {
                                //SelectList bnrlist = new SelectList("", 0);
                                ViewBag.BranchDetails = "";
                            }
                        }
                    }
                }
                
            }
            return View();
            
        }
        public async Task<ActionResult> getBranches(long BranchId)
        {
            string lang = VerifyUserSession();
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                try
                {
                    bool status = false;
                    ManageBranchesDTO brnObj = new ManageBranchesDTO();
                    brnObj.CityId = BranchId;
                    HttpResponseMessage BrnRes = await client.PostAsJsonAsync("api/BranchAPI/NewGetBranchesByCityId", brnObj);
                    if (BrnRes.IsSuccessStatusCode)
                    {

                        var brData = BrnRes.Content.ReadAsStringAsync().Result;
                        var brList = JsonConvert.DeserializeObject<List<ManageBranchesDTO>>(brData);

                        var HotelsData = BrnRes.Content.ReadAsStringAsync().Result;
                        var HotelsList = JsonConvert.DeserializeObject<List<ManageBranchesDTO>>(HotelsData);
                        List<ManageBranchesDTO> objList = HotelsList;
                        List<ManageBranchesDTO> objList1 = new List<ManageBranchesDTO>();
                        SelectList objModelData = new SelectList(objList, "Id", "NameEn", 0);
                        ViewBag.CountryList = objModelData;

                        List<ManageBranchesDTO> BranchList = JsonConvert.DeserializeObject<List<ManageBranchesDTO>>(HotelsData);
                        if (!object.Equals(BranchList, null))
                        {
                            var branches = BranchList.ToList();
                            foreach (var item in branches)
                            {
                                objList1.Add(new ManageBranchesDTO
                                {
                                    Id = item.Id,
                                    NameEn = item.NameEn
                                });
                            }
                        }
                        return Json(new SelectList(objList1, "Id", "NameEn"), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var result = "3";
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }

                    //return View("Error");
                }
                catch (Exception ex)
                {
                    ErrorLogDTO err = new ErrorLogDTO();
                    //AssignValuesToDTO.AssingDToValues(err, ex, "UsersController/Edit", "India Standard Time.");
                    //ErrorHandler.WriteError(err, ex.Message);
                    return RedirectToAction("Index");
                }
            }
        }
        public ActionResult ChangeLanguage()
        {
            String newURL = "/";
            if (Request.UrlReferrer != null)
            {
                String hostFolder = System.Configuration.ConfigurationManager.AppSettings["hostFolder"].ToString();
                String applPath = Request.Url.AbsolutePath;

                String refPath = Request.UrlReferrer.ToString();
                refPath = refPath.EndsWith("/") ? refPath : refPath + "/";
                String basePath = (Request.Url.ToString().Replace(applPath, ""));
                String userPath = refPath.Replace(hostFolder, "");

                bool arabicUser = refPath.StartsWith(hostFolder + "ar/", true, System.Globalization.CultureInfo.InvariantCulture);

                //if (arabicUser == true)
                //{
                    newURL = hostFolder + userPath.Substring(3);
                //}
                //else
                //{
                //newURL = hostFolder + "ar/" + userPath;
                //}
                //newURL = hostFolder + (arabicUser == true ? userPath.Substring(3) : "ar/" + userPath);
                return Redirect(newURL);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}