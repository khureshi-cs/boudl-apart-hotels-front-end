﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BoudlHotel
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "GalleryGroup",
               url: "Gallery/{name}",
               defaults: new { controller = "Home", action = "Gallery" },
               namespaces: new[] { "BoudlHotel.Controllers" }
           );
            routes.MapRoute(
               name: "GalleryGroup1",
               url: "Branches/{name}",
               defaults: new { controller = "Home", action = "Branches" },
               namespaces: new[] { "BoudlHotel.Controllers" }
           );
            routes.MapRoute(
              name: "NewsDetails",
              url: "News/{name}",
              defaults: new { controller = "News", action = "News" },
              namespaces: new[] { "BoudlHotel.Controllers" }
          );
            routes.MapRoute(
              name: "OffersDetails",
              url: "Offers/{name}",
              defaults: new { controller = "LatestOffers", action = "Offers" },
              namespaces: new[] { "BoudlHotel.Controllers" }
          );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "BoudlHotel.Controllers" }
            );
        }
    }
}
