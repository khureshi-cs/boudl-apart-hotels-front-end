﻿using BoudlHotel.Header;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace BoudlHotel.Controllers
{
    public class GalleryController : Controller
    {
        // GET: Gallery
        //[Route("Gallery/{Id}")]
        
        //public async Task<ActionResult> Index(Int64 id, String slug)
        //{
        //    using (HttpClient client = new HttpClient())
        //    {

        //        CommonHeader.setHeaders(client);
        //        ManageDistrictsDTO Obj = new ManageDistrictsDTO();
        //        Obj.Id = 7;
        //        //Obj.NameEn = "Palastine";
        //        //Obj.NameEn = brnName;
        //        //Obj.NameEn = "Palastine";
        //        Obj.NameEn = slug;
        //        List<ManageGalleryDTO> galleryList = new List<ManageGalleryDTO>();
        //        string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
        //        ViewBag.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
        //        HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", Obj);
        //        //HttpResponseMessage BannerRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBannerService", Obj);
        //        //cou = img + "/" + DataRow.Field<string>("SourcePath")
        //        if (Ctres.IsSuccessStatusCode)
        //        {
        //            var bnrData = Ctres.Content.ReadAsStringAsync().Result;
        //            var banners = JsonConvert.DeserializeObject<ManageGalleryDTO>(bnrData);
        //            var data = banners.datasetxml;
        //            if (data != null)
        //            {
        //                var doc = new XmlDocument();
        //                doc.LoadXml(data);
        //                DataSet ds = new DataSet();
        //                ds.ReadXml(new XmlNodeReader(doc));
        //                if (ds.Tables.Count > 0)
        //                {
        //                    if (ds.Tables[9].Rows.Count > 0)
        //                    {
        //                        galleryList = ds.Tables[9].AsEnumerable().Select(DataRow => new ManageGalleryDTO
        //                        {
        //                            Id = DataRow.Field<long>("Id"),
        //                            HotelId = DataRow.Field<long>("HotelId"),
        //                            BranchId=DataRow.Field<long>("BranchId"),
        //                            BranchName=DataRow.Field<string>("BranchName"),
        //                            ImagePath= img + "/" + DataRow.Field<string>("ImagePath")

        //                        }).ToList();
        //                        ViewBag.Galleries = galleryList;
        //                    }
        //                    else
        //                    {
        //                        SelectList bnrlist = new SelectList("", 0);
        //                        ViewBag.Galleries = bnrlist;
        //                    }
        //                }
        //            }
        //        }


        //        //ManageGalleryDTO gallery = new ManageGalleryDTO();
        //        ////gallery.BranchName = BrnName;
        //        //gallery.BranchName = "Al Khobar";
        //        //gallery.HotelId = 7;
        //        //HttpResponseMessage res = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/getGalleryImagesService", gallery);
        //        //string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
        //        //ViewBag.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
               
        //    }
        //        return View();
        //}
        [Route("Gallery")]
        public async Task<ActionResult> Index(string name)
        {
            using (HttpClient client = new HttpClient())
            {

                CommonHeader.setHeaders(client);
                ManageDistrictsDTO Obj = new ManageDistrictsDTO();
                Obj.Id = 7;
                //Obj.NameEn = "Palastine";
                //Obj.NameEn = brnName;
                List<ManageGalleryDTO> galleryList = new List<ManageGalleryDTO>();
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                ViewBag.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", Obj);
                //HttpResponseMessage BannerRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBannerService", Obj);
                //cou = img + "/" + DataRow.Field<string>("SourcePath")
                if (Ctres.IsSuccessStatusCode)
                {
                    var bnrData = Ctres.Content.ReadAsStringAsync().Result;
                    var banners = JsonConvert.DeserializeObject<ManageGalleryDTO>(bnrData);
                    var data = banners.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[9].Rows.Count > 0)
                            {
                                galleryList = ds.Tables[9].AsEnumerable().Select(DataRow => new ManageGalleryDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    HotelId = DataRow.Field<long>("HotelId"),
                                    BranchId = DataRow.Field<long>("BranchId"),
                                    BranchName = DataRow.Field<string>("BranchName"),
                                    ImagePath = img + "/" + DataRow.Field<string>("ImagePath")

                                }).ToList();
                                ViewBag.Galleries = galleryList;
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.Galleries = bnrlist;
                            }
                        }
                    }
                }


                //ManageGalleryDTO gallery = new ManageGalleryDTO();
                ////gallery.BranchName = BrnName;
                //gallery.BranchName = "Al Khobar";
                //gallery.HotelId = 7;
                //HttpResponseMessage res = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/getGalleryImagesService", gallery);
                //string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                //ViewBag.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();

            }
            return View();
        }
    }
}