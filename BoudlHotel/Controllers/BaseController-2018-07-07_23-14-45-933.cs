﻿using BoudlHotel.Header;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace BoudlHotel.Controllers
{
    [RoutePrefix("Gallery")]
    public class BaseController : Controller
    {
        public string VerifyUserSession()
        {
            if (Session["Language"] != null)
            {
                if (Session["Language"].ToString() == "En")
                {
                    return "En";
                }
                else
                {
                    return "Ar";
                }
            }
            else
            {
                Session["Language"] = "En";
                return "En";
            }
        }

    }
}