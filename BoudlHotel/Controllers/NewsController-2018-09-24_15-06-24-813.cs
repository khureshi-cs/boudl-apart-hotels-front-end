﻿using BoudlHotel.Header;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace BoudlHotel.Controllers
{
    public class NewsController : BaseController
    {
        // GET: news
        public async Task<ActionResult> Index()
        {
            string lang = VerifyUserSession();

            using (HttpClient client = new HttpClient())
            {

                CommonHeader.setHeaders(client);
                ManageNewsDTO Obj = new ManageNewsDTO();
                Obj.Id = 7;
                Obj.NameEn = "null";
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                List<ManageNewsDTO> NewsList = new List<ManageNewsDTO>();
                HttpResponseMessage NewsRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/NewsService", Obj);
                if(NewsRes.IsSuccessStatusCode)
                {
                    var NewsData = NewsRes.Content.ReadAsStringAsync().Result;
                    var News = JsonConvert.DeserializeObject<ManageNewsDTO>(NewsData);
                    var data = News.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                NewsList = ds.Tables[0].AsEnumerable().Select(DataRow => new ManageNewsDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    NameEn = DataRow.Field<string>("NameEn"),
                                    NameAr=DataRow.Field<string>("NameAr"),
                                    CreatedOn=DataRow.Field<DateTime>("CreatedOn"),
                                    ImagePath = img + "/" + DataRow.Field<string>("ImagePath")
                                }).ToList();
                                ViewBag.NewsList = NewsList;
                            }
                            else
                            {
                                SelectList newslist = new SelectList("", 0);
                                ViewBag.NewsList = newslist;
                            }
                        }
                    }
                }
            }
            //return View();
            if (lang == "En")
            {
                return View();
            }
            else
            {
                return View("Index_Ar");
            }
        }
        public async Task<ActionResult> News(string name)
        {
            string lang = VerifyUserSession();

            using (HttpClient client = new HttpClient())
            {

                CommonHeader.setHeaders(client);
                ManageNewsDTO Obj = new ManageNewsDTO();
                Obj.Id = 7;
                Obj.NameEn = name;
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                List<ManageNewsDTO> NewsDetails = new List<ManageNewsDTO>();
                HttpResponseMessage NewsRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/newsDetailService", Obj);
                if (NewsRes.IsSuccessStatusCode)
                {
                    var bnrData = NewsRes.Content.ReadAsStringAsync().Result;
                    var banners = JsonConvert.DeserializeObject<ManageNewsDTO>(bnrData);
                    var data = banners.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                NewsDetails = ds.Tables[0].AsEnumerable().Select(DataRow => new ManageNewsDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    NameEn= DataRow.Field<string>("NameEn"),
                                    NameAr = DataRow.Field<string>("NameAr"),
                                    DescEn = DataRow.Field<string>("DescEn"),
                                    DescAr = DataRow.Field<string>("DescAr"),
                                    CreatedOn = DataRow.Field<DateTime>("CreatedOn"),
                                    ImagePath = img + "/" + DataRow.Field<string>("ImagePath")

                                }).ToList();
                                ViewBag.newsDetail = NewsDetails;
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.newsDetail = bnrlist;
                            }
                        }
                    }
                }


                //ManageGalleryDTO gallery = new ManageGalleryDTO();
                ////gallery.BranchName = BrnName;
                //gallery.BranchName = "Al Khobar";
                //gallery.HotelId = 7;
                //HttpResponseMessage res = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/getGalleryImagesService", gallery);
                //string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                //ViewBag.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();

            }
            //return View();
            if (lang == "En")
            {
                return View();
            }
            else
            {
                return View("News_Ar");
            }

        }
    }
}