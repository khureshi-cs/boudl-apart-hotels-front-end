﻿using BoudlHotel.Header;
using Newtonsoft.Json;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace BoudlHotel.Controllers
{
    public class NewsController : BaseController
    {
        // GET: news
        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {

                CommonHeader.setHeaders(client);
                ManageNewsDTO Obj = new ManageNewsDTO();
                Obj.Id = 7;
                Obj.NameEn = "null";
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                List<ManageNewsDTO> NewsList = new List<ManageNewsDTO>();
                HttpResponseMessage NewsRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/NewsService", Obj);
                if(NewsRes.IsSuccessStatusCode)
                {
                    var NewsData = NewsRes.Content.ReadAsStringAsync().Result;
                    var News = JsonConvert.DeserializeObject<ManageNewsDTO>(NewsData);
                    var data = News.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                NewsList = ds.Tables[0].AsEnumerable().Select(DataRow => new ManageNewsDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    NameEn = DataRow.Field<string>("NameEn"),
                                    NameAr=DataRow.Field<string>("NameAr"),
                                    CreatedOn=DataRow.Field<DateTime>("CreatedOn"),
                                    ImagePath = img + "/" + DataRow.Field<string>("ImagePath")
                                }).ToList();
                                ViewBag.NewsList = NewsList;
                                TempData["News"] = NewsList;
                            }
                            else
                            {
                                SelectList newslist = new SelectList("", 0);
                                ViewBag.NewsList = newslist;
                                TempData["News"] = NewsList;
                            }
                        }
                    }
                }
                ManageDistrictsDTO Obj1 = new ManageDistrictsDTO();
                Obj1.Id = 7;
                List<PageHeaderDTO> pageheaderlist = new List<PageHeaderDTO>();
                HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", Obj1);
                if (Ctres.IsSuccessStatusCode)
                {
                    #region Page Header Banner

                    var CtData = Ctres.Content.ReadAsStringAsync().Result;

                    var categories = JsonConvert.DeserializeObject<ManageDistrictsDTO>(CtData);
                    var data = categories.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {

                            //Page Header Banners
                            if (ds.Tables[15].Rows.Count > 0)
                            {
                                pageheaderlist = ds.Tables[15].AsEnumerable().Select(DataRow => new PageHeaderDTO
                                {
                                    HotelId = DataRow.Field<long>("HotelId"),
                                    CustomerMenuId = DataRow.Field<long>("CustomerMenuId"),
                                    MenuNameEn = DataRow.Field<string>("MenuNameEn"),
                                    MenuNameAr = DataRow.Field<string>("MenuNameAr"),
                                    ImagePath = DataRow.Field<string>("ImagePath"),
                                })
                                .Where(x => x.MenuNameEn.ToLower() == "news")
                                .ToList();
                                //ViewBag.PageHeader = pageheaderlist;
                                ViewBag.PageHeaderBanner = img + pageheaderlist.Select(x => x.ImagePath).FirstOrDefault();
                                ViewBag.PageHeaderClass = "";
                                //System.Web.HttpContext.Current.Session["PageHeader"] = pageheaderlist;
                            }
                            else
                            {
                                ViewBag.PageHeaderBanner = "";
                                ViewBag.PageHeaderClass = " gallery-header";
                                //System.Web.HttpContext.Current.Session["PageHeader"] = null;
                            }
                        }
                    }
                    #endregion
                }
            }
            if (Session["PageHeader"] != null)
            {
                List<PageHeaderDTO> pageHeaders = ((List<PageHeaderDTO>)Session["PageHeader"]).ToList();
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                PageHeaderDTO pageHeader = pageHeaders.Where(x => x.MenuNameEn.ToLower() == "news").FirstOrDefault();
                if (pageHeader != null)
                {
                    ViewBag.PageHeaderBanner1 = img + pageHeader.ImagePath;
                    ViewBag.PageHeaderClass = "";
                }
                else
                {
                    ViewBag.PageHeaderBanner1 = "";
                    ViewBag.PageHeaderClass = " gallery-header";
                }
            }
            return View();
        }
        public async Task<ActionResult> NewPage(int page = 1, int pagsize = 2)
        {
            List<ManageNewsDTO> NewsList = new List<ManageNewsDTO>();

            NewsList = (List<ManageNewsDTO>)TempData["News"];
            PagedList<ManageNewsDTO> model = new PagedList<ManageNewsDTO>(NewsList, page, pagsize);
            return PartialView("NewPage", model);
        }
        public async Task<ActionResult> News(string name)
        {
            using (HttpClient client = new HttpClient())
            {

                CommonHeader.setHeaders(client);
                ManageNewsDTO Obj = new ManageNewsDTO();
                Obj.Id = 7;
                Obj.NameEn = name;
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                List<ManageNewsDTO> NewsDetails = new List<ManageNewsDTO>();
                HttpResponseMessage NewsRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/newsDetailService", Obj);
                if (NewsRes.IsSuccessStatusCode)
                {
                    var bnrData = NewsRes.Content.ReadAsStringAsync().Result;
                    var banners = JsonConvert.DeserializeObject<ManageNewsDTO>(bnrData);
                    var data = banners.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                NewsDetails = ds.Tables[0].AsEnumerable().Select(DataRow => new ManageNewsDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    NameEn= DataRow.Field<string>("NameEn"),
                                    NameAr = DataRow.Field<string>("NameAr"),
                                    DescEn = DataRow.Field<string>("DescEn"),
                                    DescAr = DataRow.Field<string>("DescAr"),
                                    CreatedOn = DataRow.Field<DateTime>("CreatedOn"),
                                    ImagePath = img + "/" + DataRow.Field<string>("ImagePath")

                                }).ToList();
                                ViewBag.newsDetail = NewsDetails;
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.newsDetail = bnrlist;
                            }
                        }
                    }
                }
            }
            if (Session["PageHeader"] != null)
            {
                List<PageHeaderDTO> pageHeaders = ((List<PageHeaderDTO>)Session["PageHeader"]).ToList();
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                PageHeaderDTO pageHeader = pageHeaders.Where(x => x.MenuNameEn.ToLower() == "news").FirstOrDefault();
                if (pageHeader != null)
                {
                    ViewBag.PageHeaderBanner = img + pageHeader.ImagePath;
                    ViewBag.PageHeaderClass = "";
                }
                else
                {
                    ViewBag.PageHeaderBanner = "";
                    ViewBag.PageHeaderClass = " gallery-header";
                }
            }
            return View();
        }
    }
}