﻿using BoudlHotel.Header;
using BoudlHotel.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace BoudlHotel.Controllers
{
    public class ContactController : BaseController
    {
        public static string succss = string.Empty;
        public static string Error = string.Empty;
        // GET: Contact
        public ActionResult Index()
        {
            if (Session["PageHeader"] != null)
            {
                List<PageHeaderDTO> pageHeaders = ((List<PageHeaderDTO>)Session["PageHeader"]).ToList();
                string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                PageHeaderDTO pageHeader = pageHeaders.Where(x => x.MenuNameEn.ToLower() == "contactus").FirstOrDefault();
                if (pageHeader != null)
                {
                    ViewBag.PageHeaderBanner = img + pageHeader.ImagePath;
                    ViewBag.PageHeaderClass = "";
                }
                else
                {
                    ViewBag.PageHeaderBanner = "";
                    ViewBag.PageHeaderClass = " gallery-header";
                }
            }
            return View();
            
        }
        [HttpPost]
        public async Task<ActionResult> Index(ContactRequestDTO obj)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                try
                {
                    //ContactRequestDTO ContObj = new ContactRequestDTO();
                    //ContObj.HotelId = 7;
                    obj.HotelId = 7;
                    sendMail(obj);
                    //if()
                    HttpResponseMessage CtReq = await client.PostAsJsonAsync("api/ContactAPI/AddContact", obj);
                    if (CtReq.IsSuccessStatusCode)
                    {
                        var responseData = CtReq.Content.ReadAsStringAsync().Result;
                        var res = JsonConvert.DeserializeObject<ContactRequestDTO>(responseData);
                        string msg = res.MsgStatus;
                        if (msg == "1")
                        {
                            succss = "Contact Request Saved Successfully";
                            ViewData["Success"] = succss.ToString();
                        }
                            

                        else if (msg == "4")
                        {
                            TempData["ErrorData"] = obj;
                            Error = "Failed to Save Contact Request";
                            ViewData["Error"] = Error.ToString();
                        }
                    }
                    else
                    {
                        TempData["ErrorData"] = obj;
                        Error = "Failed to Insert/Update Contact Request";
                    }

                    if (Session["PageHeader"] != null)
                    {
                        List<PageHeaderDTO> pageHeaders = ((List<PageHeaderDTO>)Session["PageHeader"]).ToList();
                        string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
                        PageHeaderDTO pageHeader = pageHeaders.Where(x => x.MenuNameEn.ToLower() == "contactus").FirstOrDefault();
                        if (pageHeader != null)
                        {
                            ViewBag.PageHeaderBanner = img + pageHeader.ImagePath;
                            ViewBag.PageHeaderClass = "";
                        }
                        else
                        {
                            ViewBag.PageHeaderBanner = "";
                            ViewBag.PageHeaderClass = " gallery-header";
                        }
                    }
                    return View();
                    
                }
                catch(Exception ex)
                {
                    Error = ex.Message;
                    return RedirectToAction("Index","Contact");
                }
            }
            //return View();
        }
        private void sendMail(ContactRequestDTO _Contact)
        {
            string mailTo = _Contact.Email;
            string mailfrom = System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString();

            string mailBCC = string.Empty;
            
            string logoPath = System.Configuration.ConfigurationManager.AppSettings["mailLogo"].ToString();

            string URL = System.Configuration.ConfigurationManager.AppSettings["URL"].ToString();

            string Subject = "Contact Us Query";
            
            string strHTML = @"<html><body><div align='center' style='width: 570px; font-family: Verdana, Geneva, sans-serif;
                                border: 1px solid #9C0606   display: block;'>
                                <table style='width: 570px; width: 51px; height: 28px; margin: 0px; border: 1px solid #9C0606'>
                                <tr><td><table style='margin-left: 0px; width: 570px;'><tr><td align='left'><a href='" + URL + @"' target='_blank'>
                                <img src='" + logoPath + @"' alt='Globus Logistics' width='240px' height='120px' style='border: 0px' />
                                </a></td></tr></table></td></tr><tr><td><hr /></td></tr><tr><td>
                                <table border='0px' cellpadding='0px' cellspacing='0px' style='width: 570px;'>
                                <tr style='height: 10px;'><td></td></tr><tr><td align='left'>
                                <font face='Verdana' style='font-size: 11px'>Dear <b>Admin</b>,</font>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'>
                                <font face='Verdana' style='font-size: 11px'>The following customer submitted a query from the website</font>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'><p align='justify'>
                                <font face='Verdana' style='font-size: 11px'><strong>Name:</strong> " + _Contact.FullName + @"<br />
                                <strong>Email:</strong> " + _Contact.Email + @" <br /><strong>Mobile:</strong> " + _Contact.Phone + @"<br />
                                <strong>Comment:</strong> <br />" + _Contact.Message + @" </font></p></td></tr><tr><td style='height: 8px;'>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'>
                                <p><font face='Verdana' style='font-size: 11px'>
                                As this is an automated response, please do not reply to this email.</font>
                                </p></td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='right'>
                                <font face='Verdana' style='font-size: 11px'>Info Team</font>
                                </td></tr><tr style='height: 10px;'><td></td></tr></table></td></tr></table>
                                <br /></div></body></html>";
            string ClientHTML = @"<html><body><div align='center' style='width: 570px; font-family: Verdana, Geneva, sans-serif;
                                border: 1px solid #9C0606   display: block;'>
                                <table style='width: 570px; width: 51px; height: 28px; margin: 0px; border: 1px solid #9C0606'>
                                <tr><td><table style='margin-left: 0px; width: 570px;'><tr><td align='left'><a href='" + URL + @"' target='_blank'>
                                <img src='" + logoPath + @"' alt='Globus Logistics' width='240px' height='120px' style='border: 0px' />
                                </a></td></tr></table></td></tr><tr><td><hr /></td></tr><tr><td>
                                <table border='0px' cellpadding='0px' cellspacing='0px' style='width: 570px;'>
                                <tr style='height: 10px;'><td></td></tr><tr><td align='left'>
                                <font face='Verdana' style='font-size: 11px'>Dear <strong>" + _Contact.FullName + @"</strong>,</font>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'>
                                <font face='Verdana' style='font-size: 11px'>Your Request Query has been placed successfully, Our Executive will contact you Shortly.</font>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'><p align='justify'>
                                <font face='Verdana' style='font-size: 11px'><strong>Name:</strong> " + _Contact.FullName + @"<br />
                                <strong>Email:</strong> " + _Contact.Email + @" <br /><strong>Mobile:</strong> " + _Contact.Phone + @"<br />
                                <strong>Comment:</strong> <br />" + _Contact.Message + @" </font></p></td></tr><tr><td style='height: 8px;'>
                                </td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='left'>
                                <p><font face='Verdana' style='font-size: 11px'>
                                As this is an automated response, please do not reply to this email.</font>
                                </p></td></tr><tr><td style='height: 8px;'></td></tr><tr><td align='right'>
                                <font face='Verdana' style='font-size: 11px'>Info Team</font>
                                </td></tr><tr style='height: 10px;'><td></td></tr></table></td></tr></table>
                                <br /></div></body></html>";
            Common.sendEmail(mailfrom, mailTo, strHTML, ClientHTML, Subject, mailBCC);
        }
        //public async Task<ActionResult> Index()
        //{
        //    using (HttpClient client = new HttpClient())
        //    {
        //        CommonHeader.setHeaders(client);
        //        ManageDistrictsDTO Obj = new ManageDistrictsDTO();
        //        Obj.Id = 7;
        //        List<ManageCitiesDTO> citylst = new List<ManageCitiesDTO>();
        //        List<ManageBranchesDTO> branchList = new List<ManageBranchesDTO>();
        //        List<ManageWeeklyOffersDTO> weeklyOffers = new List<ManageWeeklyOffersDTO>();
        //        List<HomePageAboutSectionDTO> aboutList = new List<HomePageAboutSectionDTO>();
        //        List<HotelBannersDTO> bannersList = new List<HotelBannersDTO>();
        //        string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
        //        ViewBag.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
        //        //HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService/?Id=7", Id );
        //        HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", Obj);
        //        HttpResponseMessage BannerRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBannerService", Obj);
        //        if (BannerRes.IsSuccessStatusCode)
        //        {
        //            var bnrData = BannerRes.Content.ReadAsStringAsync().Result;
        //            var banners = JsonConvert.DeserializeObject<ManageDistrictsDTO>(bnrData);
        //            var data = banners.datasetxml;
        //            if (data != null)
        //            {
        //                var doc = new XmlDocument();
        //                doc.LoadXml(data);
        //                DataSet ds = new DataSet();
        //                ds.ReadXml(new XmlNodeReader(doc));
        //                if (ds.Tables.Count > 0)
        //                {
        //                    if (ds.Tables[0].Rows.Count > 0)
        //                    {
        //                        bannersList = ds.Tables[0].AsEnumerable().Select(DataRow => new HotelBannersDTO
        //                        {
        //                            Id = DataRow.Field<long>("Id"),
        //                            MediaTypeIdId = DataRow.Field<long>("MediaTypeId"),
        //                            SourcePath = img + "/" + DataRow.Field<string>("SourcePath")
        //                        }).ToList();
        //                        ViewBag.MediaType = ds.Tables[0].Rows[0]["MediaTypeId"].ToString();
        //                        ViewBag.BannersList = bannersList;
        //                    }
        //                    else
        //                    {
        //                        SelectList bnrlist = new SelectList("", 0);
        //                        ViewBag.BannersList = bnrlist;
        //                    }
        //                }
        //            }
        //        }
        //        if (Ctres.IsSuccessStatusCode)
        //        {
        //            var CtData = Ctres.Content.ReadAsStringAsync().Result;
        //            //=====================================================================
        //            //var Ctlist = JsonConvert.DeserializeObject<List<object>>(CtData);
        //            //List<object> objList = Ctlist;
        //            //SelectList objModelData = new SelectList(objList, "Id", "NameEn", 0);
        //            //ViewBag.CityList = objModelData;//BranchList
        //            //=====================================================================
        //            var categories = JsonConvert.DeserializeObject<ManageDistrictsDTO>(CtData);
        //            var data = categories.datasetxml;
        //            if (data != null)
        //            {
        //                var doc = new XmlDocument();
        //                doc.LoadXml(data);
        //                DataSet ds = new DataSet();
        //                ds.ReadXml(new XmlNodeReader(doc));
        //                if (ds.Tables.Count > 0)
        //                {
        //                    //Hotel cities Binding
        //                    if (ds.Tables[0].Rows.Count > 0)
        //                    {
        //                        citylst = ds.Tables[0].AsEnumerable().Select(DataRow => new ManageCitiesDTO
        //                        {
        //                            Id = DataRow.Field<long>("Id"),
        //                            NameEn = DataRow.Field<string>("NameEn"),
        //                            NameAr = DataRow.Field<string>("NameAr")
        //                        }).ToList();
        //                        ViewBag.cityList = new SelectList(citylst, "Id", "NameEn", 0);

        //                    }
        //                    else
        //                    {
        //                        SelectList citlist = new SelectList("", 0);
        //                        ViewBag.cityList = citlist;
        //                    }
        //                    // Hotel Branches Binding here
        //                    if (ds.Tables[1].Rows.Count > 0)
        //                    {
        //                        branchList = ds.Tables[1].AsEnumerable().Select(DataRow => new ManageBranchesDTO
        //                        {
        //                            BookingUrl = DataRow.Field<string>("BookingUrl"),
        //                            CityId = DataRow.Field<long>("CityId"),
        //                            NameEn = DataRow.Field<string>("NameEn"),
        //                            NameAr = DataRow.Field<string>("NameAr")
        //                        }).ToList();
        //                        //ViewBag.BrnList = new SelectList(branchList, "BookingUrl", "NameEn", 0);
        //                        ViewBag.BrnList1 = new SelectList(branchList);
        //                        ViewBag.BrnList = JsonConvert.SerializeObject(branchList);
        //                    }
        //                    else
        //                    {
        //                        SelectList Branchlist = new SelectList("", 0);
        //                        ViewBag.BrnList = Branchlist;
        //                    }
        //                    //Hotel Weekly Top Offers
        //                    if (ds.Tables[2].Rows.Count > 0)
        //                    {
        //                        weeklyOffers = ds.Tables[2].AsEnumerable().Select(DataRow => new ManageWeeklyOffersDTO
        //                        {
        //                            HotelId = DataRow.Field<long>("HotelId"),
        //                            HotelName = DataRow.Field<string>("HotelName"),
        //                            OfferOneImageEn = img + "/" + DataRow.Field<string>("OfferOneImageEn"),
        //                            OfferOneImageAr = img + "/" + DataRow.Field<string>("OfferOneImageAr"),
        //                            OfferTwoImageEn = img + "/" + DataRow.Field<string>("OfferTwoImageEn"),
        //                            OfferTwoImageAr = img + "/" + DataRow.Field<string>("OfferTwoImageAr"),
        //                        }).ToList();
        //                        //ViewBag.BrnList = new SelectList(branchList, "BookingUrl", "NameEn", 0);
        //                        ViewBag.BrnList1 = new SelectList(weeklyOffers);
        //                        ViewBag.WeeklyOffersList = weeklyOffers;
        //                    }
        //                    else
        //                    {
        //                        SelectList Branchlist = new SelectList("", 0);
        //                        ViewBag.WeeklyOffersList = Branchlist;
        //                    }
        //                    //Hotel Home page Aboutus Section
        //                    if (ds.Tables[3].Rows.Count > 0)
        //                    {
        //                        aboutList = ds.Tables[3].AsEnumerable().Select(DataRow => new HomePageAboutSectionDTO
        //                        {
        //                            HotelId = DataRow.Field<long>("HotelId"),
        //                            DescriptionEn = DataRow.Field<string>("DescriptionEn"),
        //                            DescriptionAr = DataRow.Field<string>("DescriptionAr"),
        //                            ImageEn = img + "/" + DataRow.Field<string>("ImageEn"),
        //                            ImageAr = img + "/" + DataRow.Field<string>("ImageAr"),
        //                        }).ToList();
        //                        ViewBag.HomeAboutList = aboutList;
        //                    }
        //                    else
        //                    {
        //                        SelectList aboutList1 = new SelectList("", 0);
        //                        ViewBag.HomeAboutList = aboutList1;
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return View();
        //}
        //public async Task<ActionResult> Index(ContactRequestDTO obj)
        //{
        //    using (HttpClient client = new HttpClient())
        //    {
        //        CommonHeader.setHeaders(client);
        //        try
        //        {

        //            return RedirectToAction("Index");
        //        }
        //        catch(Exception ex)
        //        {
        //            ErrorLogDTO err = new ErrorLogDTO();
        //           // Error = ex.Message;
        //            return RedirectToAction("Index");
        //        }
        //    }
        //}
    }
}