﻿using BoudlHotel.Header;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace BoudlHotel.Controllers
{
    
    public class BaseController : Controller
    {
        public BaseController()
        {
            //System.Web.HttpContext.Current.Session.Clear();
            Task<string> x = PrepareMenuData();
            if (System.Configuration.ConfigurationManager.AppSettings["UrlPrepend"] != null)
                System.Web.HttpContext.Current.Session["UrlPrepend"] = System.Configuration.ConfigurationManager.AppSettings["UrlPrepend"].ToString();
            else
                System.Web.HttpContext.Current.Session["UrlPrepend"] = "";
        }

        public async Task<string> PrepareMenuData()
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);

                List<ManageCitiesDTO> citylst = new List<ManageCitiesDTO>();
                List<ManageBranchesDTO> branchList = new List<ManageBranchesDTO>();
                List<ManageWeeklyOffersDTO> weeklyOffers = new List<ManageWeeklyOffersDTO>();
                List<HomePageAboutSectionDTO> aboutList = new List<HomePageAboutSectionDTO>();
                List<ManageCountriesDTO> countryList = new List<ManageCountriesDTO>();
                List<HotelBannersDTO> bannersList = new List<HotelBannersDTO>();
                List<CustomMenuDTO> HomeMenuList = new List<CustomMenuDTO>();
                List<SpecialPackage> specialPackages = new List<SpecialPackage>();
                List<HighlightsDTO> highlights = new List<HighlightsDTO>();
                List<PageHeaderDTO> pageheaderlist = new List<PageHeaderDTO>();
                ManageDistrictsDTO Obj = new ManageDistrictsDTO();
                Obj.Id = 7;
                HttpResponseMessage Ctres = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", Obj);
                if (Ctres.IsSuccessStatusCode)
                {
                    #region MenuPreparation

                    var CtData = Ctres.Content.ReadAsStringAsync().Result;
                    //=====================================================================
                    //var Ctlist = JsonConvert.DeserializeObject<List<object>>(CtData);
                    //List<object> objList = Ctlist;
                    //SelectList objModelData = new SelectList(objList, "Id", "NameEn", 0);
                    //ViewBag.CityList = objModelData;//BranchList
                    //=====================================================================
                    var categories = JsonConvert.DeserializeObject<ManageDistrictsDTO>(CtData);
                    var data = categories.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            //Hotel cities Binding
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                citylst = ds.Tables[0].AsEnumerable().Select(DataRow => new ManageCitiesDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    CountryId = DataRow.Field<long>("CountryId"),
                                    NameEn = DataRow.Field<string>("NameEn"),
                                    NameAr = DataRow.Field<string>("NameAr")
                                }).ToList();
                                ViewBag.cityList = new SelectList(citylst, "Id", "NameEn", 0);
                                Session["CitiesList"] = citylst;
                            }
                            else
                            {
                                SelectList citlist = new SelectList("", 0);
                                ViewBag.cityList = citlist;
                            }
                            // Hotel Branches Binding here
                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                branchList = ds.Tables[1].AsEnumerable().Select(DataRow => new ManageBranchesDTO
                                {
                                    Id = DataRow.Field<Int64>("Id"),
                                    BookingUrl = DataRow.Field<string>("BookingUrl"),
                                    CityId = DataRow.Field<long>("CityId"),
                                    NameEn = DataRow.Field<string>("NameEn"),
                                    NameAr = DataRow.Field<string>("NameAr")
                                }).ToList();
                                //ViewBag.BrnList = new SelectList(branchList, "BookingUrl", "NameEn", 0);
                                ViewBag.BrnList1 = new SelectList(branchList);
                                //ViewBag.BranchesList = branchList;
                                Session["BranchesList"] = branchList;
                                ViewBag.BrnList = JsonConvert.SerializeObject(branchList);
                            }
                            else
                            {
                                SelectList Branchlist = new SelectList("", 0);
                                ViewBag.BrnList = Branchlist;
                            }
                            
                            if (ds.Tables[7].Rows.Count > 0)
                            {
                                countryList = ds.Tables[7].AsEnumerable().Select(DataRow => new ManageCountriesDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    NameEn = DataRow.Field<string>("NameEn"),
                                    NameAr = DataRow.Field<string>("NameAr"),
                                }).ToList();
                                ViewBag.CountriesList = countryList;
                                Session["CountriesList"] = countryList;
                            }
                            else
                            {
                                SelectList CountryList1 = new SelectList("", 0);
                                ViewBag.CountriesList = CountryList1;
                            }
                            if (ds.Tables[8].Rows.Count > 0)
                            {
                                HomeMenuList = ds.Tables[8].AsEnumerable().Select(DataRow => new CustomMenuDTO
                                {
                                    Id = DataRow.Field<Int64>("Id"),
                                    NameEn = DataRow.Field<string>("NameEn"),
                                    NameAr = DataRow.Field<string>("NameAr"),
                                    DynamicBranches = DataRow.Field<bool>("DynamicBranches"),
                                    URL = DataRow.Field<string>("URL"),
                                    SubUrl = DataRow.Field<string>("SubUrl"),
                                    UrlAr = DataRow.Field<string>("UrlAr"),
                                    SubUrlAr = DataRow.Field<string>("SubUrlAr"),
                                    SortIndex = DataRow.Field<int>("SortIndex")
                                }).ToList();
                                ViewBag.HomeMenuList = HomeMenuList;
                                Session["HomeMenuList"] = HomeMenuList;
                            }
                            else
                            {
                                SelectList HomeMenuList1 = new SelectList("", 0);
                                ViewBag.HomeMenuList = HomeMenuList1;
                            }
                            //Page Header Banners
                            if (ds.Tables[15].Rows.Count > 0)
                            {
                                pageheaderlist = ds.Tables[15].AsEnumerable().Select(DataRow => new PageHeaderDTO
                                {
                                    HotelId = DataRow.Field<long>("HotelId"),
                                    CustomerMenuId = DataRow.Field<long>("CustomerMenuId"),
                                    MenuNameEn = DataRow.Field<string>("MenuNameEn"),
                                    MenuNameAr = DataRow.Field<string>("MenuNameAr"),
                                    ImagePath =  DataRow.Field<string>("ImagePath"),
                                })
                                //.Where(x => x.MenuNameEn == "ABOUTUS")
                                .ToList();
                                //ViewBag.PageHeader = pageheaderlist;
                                System.Web.HttpContext.Current.Session["PageHeader"] = pageheaderlist;
                            }
                            else
                            {
                                System.Web.HttpContext.Current.Session["PageHeader"] = null;
                            }
                            
                        }
                    }
                    #endregion
                }
            }

            return "";

        }

        public string VerifyUserSession()
        {
            if (Session["Language"] != null)
            {
                if (Session["Language"].ToString() == "En")
                {
                    return "En";
                }
                else
                {
                    return "Ar";
                }
            }
            else
            {
                Session["Language"] = "En";
                return "En";
            }
        }

    }
}