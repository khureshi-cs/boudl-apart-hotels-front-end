﻿using BoudlHotel.Header;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace BoudlHotel.Controllers
{
    public class CareersController : Controller
    {
        public static string succss = string.Empty;
        public static string Error = string.Empty;
        // GET: Careers
        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                CareerApplicationDTO obj = new CareerApplicationDTO();
                CommonHeader.setHeaders(client);
                obj.HotelId = 7;
                List<JobCategoriesDTO> carList = new List<JobCategoriesDTO>();
                List<CareerApplicationDTO> careerCityList = new List<CareerApplicationDTO>();
                List<CareerApplicationDTO> careerPositionList = new List<CareerApplicationDTO>();
                //HttpResponseMessage BannerRes = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBannerService", Obj);
                HttpResponseMessage res = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/CareerApplService", obj);
                if (res.IsSuccessStatusCode)
                {
                    var resData = res.Content.ReadAsStringAsync().Result;
                    var CareerData = JsonConvert.DeserializeObject<CareerApplicationDTO>(resData);
                    var data = CareerData.datasetxml;
                    if (data != null)
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(doc));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                carList = ds.Tables[0].AsEnumerable().Select(DataRow => new JobCategoriesDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    NameEn=DataRow.Field<string>("NameEn"),
                                    NameAr=DataRow.Field<string>("NameAr")
                                }).ToList();
                                
                                //SelectList objModelData = new SelectList(carList, "Id", "NameEn", 0);
                                //ViewBag.JobCatList = objModelData;
                                ViewBag.JobCatList = JsonConvert.SerializeObject(carList);
                                // ViewBag.JobCatList = carList;
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.JobCatList = bnrlist;
                            }
                            if (ds.Tables[2].Rows.Count > 0)
                            {
                                careerCityList = ds.Tables[2].AsEnumerable().Select(DataRow => new CareerApplicationDTO
                                {
                                   
                                    JobCategoryId=DataRow.Field<long>("JobCategoryId"),
                                    CityId=DataRow.Field<long>("CityId"),
                                    NameEn=DataRow.Field<string>("NameEn"),
                                    NameAr = DataRow.Field<string>("NameAr")
                                }).ToList();
                                //ViewBag.Positions = careerList;PositionList
                                ViewBag.CityList =JsonConvert.SerializeObject(careerCityList);
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.Positions = bnrlist;
                            }
                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                careerPositionList = ds.Tables[1].AsEnumerable().Select(DataRow => new CareerApplicationDTO
                                {
                                    Id = DataRow.Field<long>("Id"),
                                    JobCategoryId = DataRow.Field<long>("JobCategoryId"),
                                    PositionEn = DataRow.Field<string>("PositionEn"),
                                    PositionAr = DataRow.Field<string>("PositionAr"),
                                    CityId = DataRow.Field<long>("CityId")
                                    
                                }).ToList();
                                //ViewBag.Positions = careerList;
                                ViewBag.PositionList = JsonConvert.SerializeObject(careerPositionList);
                            }
                            else
                            {
                                SelectList bnrlist = new SelectList("", 0);
                                ViewBag.PositionList = bnrlist;
                            }
                        }
                    }
                }
            }
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Index(CareerApplicationDTO obj)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                try
                {
                    //ContactRequestDTO ContObj = new ContactRequestDTO();
                    //ContObj.HotelId = 7;
                    obj.HotelId = 7;
                    //sendMail(obj);
                    //if()
                    HttpResponseMessage CtReq = await client.PostAsJsonAsync("api/ContactAPI/AddContact", obj);
                    if (CtReq.IsSuccessStatusCode)
                    {
                        var responseData = CtReq.Content.ReadAsStringAsync().Result;
                        var res = JsonConvert.DeserializeObject<ContactRequestDTO>(responseData);
                        string msg = res.MsgStatus;
                        if (msg == "1")
                        {
                            succss = "Career Request Saved Successfully";
                            ViewData["Success"] = succss.ToString();
                        }


                        else if (msg == "4")
                        {
                            TempData["ErrorData"] = obj;
                            Error = "Failed to Save Career Request";
                            ViewData["Error"] = Error.ToString();
                        }
                    }
                    else
                    {
                        TempData["ErrorData"] = obj;
                        Error = "Failed to Insert/Update Career Request";
                    }
                    //return RedirectToAction("Index","Contact");
                    return View();
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return RedirectToAction("Index", "Careers");
                }
            }
            //return View();
        }
        //public async Task<ActionResult> getCities(long JobCatId)
        //{
        //    using (HttpClient client = new HttpClient())
        //    {
        //        CommonHeader.setHeaders(client);
        //        try
        //        {
        //            bool status = false;
        //            CareerApplicationDTO obj = new CareerApplicationDTO();
        //            List<CareerApplicationDTO> careerList = new List<CareerApplicationDTO>();
        //            obj.HotelId = 7;
        //            obj.JobCategoryId = JobCatId;
        //            HttpResponseMessage res = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/CareerApplService", obj);
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var resData = res.Content.ReadAsStringAsync().Result;
        //                var CareerData = JsonConvert.DeserializeObject<CareerApplicationDTO>(resData);
        //                var data = CareerData.datasetxml;
        //                if (data != null)
        //                {
        //                    var doc = new XmlDocument();
        //                    doc.LoadXml(data);
        //                    DataSet ds = new DataSet();
        //                    ds.ReadXml(new XmlNodeReader(doc));
        //                    if (ds.Tables.Count > 0)
        //                    {
        //                        if (ds.Tables[2].Rows.Count > 0)
        //                        {
        //                            careerList = ds.Tables[2].AsEnumerable().Select(DataRow => new CareerApplicationDTO
        //                            {
        //                                Id = DataRow.Field<long>("Id"),
        //                                HotelId = DataRow.Field<long>("HotelId"),
        //                                JobCategoryId = DataRow.Field<long>("JobCategoryId"),
        //                                PositionEn = DataRow.Field<string>("PositionEn"),
        //                                PositionAr = DataRow.Field<string>("PositionAr"),
        //                                CityId = DataRow.Field<long>("CityId"),
        //                                NameEn = DataRow.Field<string>("NameEn"),
        //                                NameAr = DataRow.Field<string>("NameAr")
        //                            }).ToList();
        //                            //ViewBag.Positions = careerList;
        //                            ViewBag.Positions = JsonConvert.SerializeObject(careerList);
        //                        }
        //                        else
        //                        {
        //                            SelectList bnrlist = new SelectList("", 0);
        //                            ViewBag.Positions = bnrlist;
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                var result = "3";
        //                return Json(result, JsonRequestBehavior.AllowGet);
        //            }

        //            //return View("Error");
        //        }
        //        catch (Exception ex)
        //        {
        //            ErrorLogDTO err = new ErrorLogDTO();
        //            //AssignValuesToDTO.AssingDToValues(err, ex, "UsersController/Edit", "India Standard Time.");
        //            //ErrorHandler.WriteError(err, ex.Message);
        //            return RedirectToAction("Error");
        //        }
        //    }
        //}
    }
}