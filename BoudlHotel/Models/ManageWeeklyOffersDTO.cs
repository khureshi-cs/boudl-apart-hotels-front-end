﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoudlHotel
{
    public class ManageWeeklyOffersDTO
    {
        public Int64 Id { get; set; }
        public Int64 HotelId { get; set; }
        public string HotelName { get; set; }
        public Int64 OfferOne { get; set; }
        public Int64 OfferTwo { get; set; }
        public string OfferOneNameEn { get; set; }
        public string OfferOneNameAr { get; set; }
        public string OfferTwoNameEn { get; set; }
        public string OfferTwoNameAr { get; set; }
        public string OfferOneImageEn { get; set; }
        public string OfferOneImageAr { get; set; }
        public string OfferTwoImageEn { get; set; }
        public string OfferTwoImageAr { get; set; }
        public bool isActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DeletedBy { get; set; }
        public bool isDeleted { get; set; }
        public IEnumerable<ManageWeeklyOffersDTO> WeeklyOfferList { get; set; }
        public IEnumerable<ManageHotelsDTO> HotelList { get; set; }
        public IEnumerable<ManageOffersDTO> offersList { get; set; }
        public string message { get; set; }
        public int FlagId { get; set; }

    }
}