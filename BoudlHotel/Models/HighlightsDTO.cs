﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoudlHotel
{
    public class HighlightsDTO
    {
        public Int64 Id { get; set; }
        public Int64 HotelId { get; set; }
        public String ImageEn { get; set; }
        public String ImageAr { get; set; }
        public String CaptionEn { get; set; }
        public String CaptionAr { get; set; }
        public Int32 SortIndex { get; set; }
    }
}