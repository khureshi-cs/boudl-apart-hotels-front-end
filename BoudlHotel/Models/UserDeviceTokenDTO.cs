﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoudlHotel
{
    public class UserDeviceTokenDTO
    {
        public Int64 Id { get; set; }
        public string DeviceToken { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public string message { get; set; }
       
    }
}