﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BoudlHotel
{
    public class ManageDistrictsDTO
    {
        public long Id { get; set; }
        [Display(Name = "Name (English)")]
        [Required(ErrorMessage = "*Enter Name (English)", AllowEmptyStrings = false)]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "*Max Length is 50")]
        public string NameEn { get; set; }
        [Display(Name = "Name (Arabic)")]
        [Required(ErrorMessage = "*Enter Name (Arabic)", AllowEmptyStrings = false)]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "*Max Length is 50")]
        public string NameAr { get; set; }
        [Required(ErrorMessage = "*Please Select City", AllowEmptyStrings = false)]
        public long CityId { get; set; }

        public bool isActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        public long ModifiedBy { get; set; }
        public long DeletedBy { get; set; }
        public int isDeleted { get; set; }

        public string message { get; set; }
        public int FlagId { get; set; }
        public string datasetxml { get; set; }
        [Required(ErrorMessage = "*Please Select Country", AllowEmptyStrings = false)]
        public long countryid { get; set; }
        public string countryname { get; set; }
        public string cityname { get; set; }
        public string districtname { get; set; }

        [Display(Name = "Country")]
        [Required(ErrorMessage = "*Please Select Country", AllowEmptyStrings = false)]
        public IEnumerable<ManageDistrictsDTO> Countrieslist { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "*Please Select City", AllowEmptyStrings = false)]
        public IEnumerable<ManageDistrictsDTO> CitiesList { get; set; }

        public IEnumerable<ManageDistrictsDTO> DistrictsList { get; set; }
        public long countryidforddl { get; set; }
        public long cityidforddl { get; set; }
        public string districtnameAr { get; set; }


    }
}