﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoudlHotel
{
    public class ManageOffersDTO
    {
        public Int64 Id { get; set; }
        public Int64 HotelId { get; set; }
        public Int64 CountryId { get; set; }
        public string CountryName { get; set; }
        public Int64 BranchId { get; set; }
        public string HotelName { get; set; }
        public int Mode { get; set; }
        public string OfferBookingUrl { get; set; }
        public string BookingUrl { get; set; }
        public int TypeId { get; set; }

        [Required(ErrorMessage = "*Enter Hotel Offer Name in English", AllowEmptyStrings = false)]
        //[RegularExpression(@"^[a-zA-Z0-9\s]+$", ErrorMessage = "Special characters are not allowed.")]
        //[RegularExpression(@"^[a-zA-Z0-9\s]+$", ErrorMessage = "Special characters are not allowed.")]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Max Length is 150")]
        public string NameEn { get; set; }
        [Required(ErrorMessage = "*Enter Hotel Offer Name in Arabic", AllowEmptyStrings = false)]
        //[RegularExpression(@"^[a-zA-Z0-9\s]+$", ErrorMessage = "Special characters are not allowed.")]
        //[RegularExpression(@"^[\u0621-\u064A\u0660-\u0669 a-zA-Z0-9\s]+$", ErrorMessage = "Special characters are not allowed.")]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Max Length is 150")]
        public string NameAr { get; set; }
        [Required(ErrorMessage = "*Enter Hotel Offer Description in English", AllowEmptyStrings = false)]
        public string DescriptionEn { get; set; }
        [Required(ErrorMessage = "*Enter Hotel Offer Description in Arabic", AllowEmptyStrings = false)]
        public string DescriptionAr { get; set; }
        [Required(ErrorMessage = "Please select file.")]
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif)$", ErrorMessage = "Only Image files allowed.")]
        public string ImageEn { get; set; }
        [Required(ErrorMessage = "Please select file.")]
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif)$", ErrorMessage = "Only Image files allowed.")]
        public string ImageAr { get; set; }
        [Required(ErrorMessage = "Please Enter Offer Price", AllowEmptyStrings = false)]
        [RegularExpression(@"^\d+(.\d{1,2})?$", ErrorMessage = "Invalid Offer Price")]
        //[RegularExpression(@"-?([1-9]?[0-9])\.{1}\d{1,2}", ErrorMessage = "Invalid Offer Price")]
        //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Invalid Offer Price")]
        public decimal? OfferPrice { get; set; }

        //[DataType(DataType.Date),DisplayFormat(DataFormatString =@"{0:dd\/MM\/yyyy HH:mm",ApplyFormatInEditMode =true)]

        //[DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}")]

        [Required(ErrorMessage = "*Enter Start Date")]
        public string StartDateS { get; set; }
        [Required(ErrorMessage = "*Enter End Date")]
        public string EndDateE { get; set; }
        //public DateTime? StartDate { get; set; }
        ////[DataType(DataType.Date), DisplayFormat(DataFormatString = @"{0:dd\/MM\/yyyy HH:mm", ApplyFormatInEditMode = true)]

        //public DateTime? EndDate { get; set; }
        //[DataType(DataType.Date), DisplayFormat(DataFormatString = @"{0:dd\/MM\/yyyy HH:mm", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }
        //[DataType(DataType.Date), DisplayFormat(DataFormatString = @"{0:dd\/MM\/yyyy HH:mm", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }
        public int SortIndex { get; set; }
        public string AmenityIds { get; set; }
        public bool BookingRedirection { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public int DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public IEnumerable<ManageOffersDTO> OffersList { get; set; }
        public IEnumerable<ManageHotelAmenitiesDTO> Amenities { get; set; }
        public string message { get; set; }
        public int FlagId { get; set; }
        public string datasetxml { get; set; }
        public string[] arAmnEn { get; set; }
        public string[] arAmnAr { get; set; }
        public string[] amIcons { get; set; }
        public string Currency { get; internal set; }
    }
}