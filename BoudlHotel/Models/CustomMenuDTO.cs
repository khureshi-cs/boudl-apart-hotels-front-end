﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoudlHotel
{
    public class CustomMenuDTO
    {
        public Int64 Id { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string URL { get; set; }
        public string UrlAr { get; set; }
        public string SubUrl { get; set; }
        public string SubUrlAr { get; set; }
        public bool DynamicBranches { get; set; }
        public int SortIndex { get; set; }
        
    }
}