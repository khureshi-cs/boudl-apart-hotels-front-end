﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoudlHotel
{
    public class MenuDTO
    {
        public Int64 UserId { get; set; }
        public Int64 MenuItemId { get; set; }
        public Int64 MenuCategoryId { get; set; }
        public string MenuCategoryName { get; set; }
        public string MenuName { get; set; }
        public string MenuUrl { get; set; }
        public string IconClass { get; set; }
        public bool IsActive { get; set; }
    }
}