﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoudlHotel
{
    public class CareerApplication
    {
        public Int64 Id { get; set; }
        public Int64 HotelId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Int64 JobCategoryId { get; set; }
        public Int64 PositionId { get; set; }
        public string ProfilePath { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long Createdby { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        public long ModifiedBy { get; set; }
        public long DeletedBy { get; set; }
        public bool isDeleted { get; set; }
        public string message { get; set; }
    }
}