﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoudlHotel
{
    public class UserSubscriptionDTO
    {
        public Int64 Id { get; set; }
        public Int64 HotelId { get; set; }
        public string EmailId { get; set; }
        public bool IsActive { get; set; }
        public string message { get; set; }
    }
}