﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoudlHotel
{
    public class ManageNewsDTO
    {
        public long Id { get; set; }
        //[Display(Name = "Hotel Name")]
        //[Required(ErrorMessage = "*Select Hotel Name", AllowEmptyStrings = true)]
        //public long HotelId { get; set; }
        //public IEnumerable<ManageHotelsDTO> HotelsList { get; set; }
        public int SortIndex { get; set; }
        [Display(Name = "Title (English)")]
        [Required(ErrorMessage = "*Please Enter Hotel Name (English)", AllowEmptyStrings = false)]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "* Max Length is 100")]
        public string NameEn { get; set; }
        [Display(Name = "Title (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Hotel Name (Arabic)", AllowEmptyStrings = false)]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "* Max Length is 100")]
        public string NameAr { get; set; }
        [Display(Name = "Description (English)")]
        [Required(ErrorMessage = "*Please Enter Hotel Description (English)", AllowEmptyStrings = false)]
        [AllowHtml]
        public string DescEn { get; set; }
        [Display(Name = "Description (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Hotel Description (Arabic)", AllowEmptyStrings = false)]
        [AllowHtml]
        public string DescAr { get; set; }
        public string ImagePath { get; set; }
        public string HotelNameEn { get; set; }
        public string HotelNameAr { get; set; }
        public bool isActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        public long ModifiedBy { get; set; }
        public long DeletedBy { get; set; }
        public bool isDeleted { get; set; }

        [Display(Name = "Hotel Name")]
        [Required(ErrorMessage = "*Select Hotel Name", AllowEmptyStrings = false)]
        public IEnumerable<ManageNewsDTO> hotelslist { get; set; }
        public IEnumerable<ManageNewsDTO> hotelnewslist {get;set;}
        public IEnumerable<ManageNewsDTO> HtlsList { get; set; }

        public string hotelname { get; set; }
        public string message { get; set; }
        public int FlagId { get; set; }
        public string datasetxml { get; set; }
        public string ContentType { get; set; }
        public string filepath { get; set; }
        public string Hotel { get; set; }
        public long[] aaa { get; set; }
        public bool isSelected { get; set; }
        public string hotelids { get; set; }

        public IEnumerable<SelectListItem> ddlhotel { get; set; }

    }
}