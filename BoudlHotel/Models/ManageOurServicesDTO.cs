﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace BoudlHotel
{
    public class ManageOurServicesDTO
    {
        public long Id { get; set; }
        [Display(Name = "Hotel")]
        [Required(ErrorMessage = "*{0} is required.")]
        public long HotelId { get; set; }
        [Display(Name = "Caption (English)")]
        [Required(ErrorMessage = "*Please Enter Caption (English)", AllowEmptyStrings = false)]
        public string CaptionEn { get; set; }
        [Display(Name = "Caption (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Caption (Arabic)", AllowEmptyStrings = false)]
        public string CaptionAr { get; set; }
        [Display(Name = "Image (English)")]
        [Required(ErrorMessage = "*Upload Image (English)", AllowEmptyStrings = false)]
        public string ImageEn { get; set; }
        [Display(Name = "Image (Arabic)")]
        [Required(ErrorMessage = "*Upload Image (Arabic)", AllowEmptyStrings = false)]
        public string ImageAr { get; set; }
        public int SortIndex { get; set; }

        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public long ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
        public long DeletedBy { get; set; }
        public IEnumerable<ManageOurServicesDTO> ourserviceslist { get; set; }

        public string message { get; set; }
        public int FlagId { get; set; }
        public string datasetxml { get; set; }
        public string hotelname { get; set; }
        public string ContentType { get; set; }

        public string filepath { get; set; }
        public string Hotel { get; set; }

    }
}