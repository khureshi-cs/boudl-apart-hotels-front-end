﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace BoudlHotel
{
    public class BoardMembersDTO
    {
        public Int64 Id { get; set; }
        [Display(Name = "Name (English)")]
        [Required(ErrorMessage = "*Please Enter Name (English)", AllowEmptyStrings = false)]
        public string NameEn { get; set; }
        [Display(Name = "Name (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Name (Arabic)", AllowEmptyStrings = false)]
        public string NameAr { get; set; }
        [Display(Name = "Designation (English)")]
        [Required(ErrorMessage = "*Please Enter Designation (English)", AllowEmptyStrings = false)]
        public string DesignationEn { get; set; }
        [Display(Name = "Designation (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Designation (Arabic)", AllowEmptyStrings = false)]
        public string DesignationAr { get; set; }
        public string ImagePath { get; set; }
        public bool IsBoardMember { get; set; }
        public bool HasMessage { get; set; }

        [Display(Name = "Title (English)")]
        [Required(ErrorMessage = "*Please Enter Title (English)", AllowEmptyStrings = false)]
        public string MsgTitleEn { get; set; }
        [Display(Name = "Title (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Tiltle (Arabic)", AllowEmptyStrings = false)]
        public string MsgTitleAr { get; set; }
        [Display(Name = "Message (English)")]
        [Required(ErrorMessage = "*Please Enter Message (English)", AllowEmptyStrings = false)]
        public string MessageEn { get; set; }
        [Display(Name = "Message (English)")]
        [Required(ErrorMessage = "*Please Enter Message (Arabic)", AllowEmptyStrings = false)]
        public string MessageAr { get; set; }
        public int BoardMemberSortIndex { get; set; }
        public int MessagePageSortIndex { get; set; }

        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public Int64 CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Int64 ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
        public Int64 DeletedBy { get; set; }
        public IEnumerable<BoardMembersDTO> boardmemberslist { get; set; }

        public int FlagId { get; set; }
        public string message { get; set; }

        public string datasetxml { get; set; }
        //public string imagepath { get; set; }
        public string ContentType { get; set; }
        public string filepath { get; set; }
    }
}