﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoudlHotel
{
    public class PageHeaderDTO
    {
        public int Id { get; set; }
        public Int64 HotelId { get; set; }
        public Int64 CustomerMenuId { get; set; }
        public string ImagePath { get; set; }
        public string MenuNameEn { get; set; }
        public string MenuNameAr { get; set; }
    }
}