﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace BoudlHotel
{
    public class ManageCountriesDTO
    {
        public Int64 Id { get; set; }
        [Display(Name = "Name (English)")]
        [Required(ErrorMessage = "*Please Enter Country Name (English)", AllowEmptyStrings = false)]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "*Max Length is 50")]
        public string NameEn { get; set; }
        [Display(Name = "Name (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Country Name (Arabic)", AllowEmptyStrings = false)]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "*Max Length is 50")]
        public string NameAr { get; set; }
        [Display(Name = "Code (English)")]
        [Required(ErrorMessage = "*Please Enter Country Code (English)", AllowEmptyStrings = false)]
        [StringLength(10, MinimumLength = 1, ErrorMessage = "*Max Length is 10")]
        public string CodeEn { get; set; }
        [Display(Name = "Code (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Code (Arabic)", AllowEmptyStrings = false)]
        [StringLength(10, MinimumLength = 1, ErrorMessage = "*Max Length is 10")]
        public string CodeAr { get; set; }
        public string Icon { get; set; }
        [Display(Name = "Currency")]
        [Required(ErrorMessage = "*Please Enter Currency", AllowEmptyStrings = false)]
        [StringLength(10, MinimumLength = 1, ErrorMessage = "*Max Length is 10")]
        public string DefaultCurrency { get; set; }
        public string CurrencyCode { get; set; }
        public bool isSelected { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Int64 ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DeletedBy { get; set; }

        public IEnumerable<ManageCountriesDTO> Countrieslist { get; set; }

        public int FlagId { get; set; }
        public string message { get; set; }
        public string checkduplicate { get; set; }
    }
}