﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoudlHotel
{
    public class JobCategoriesDTO
    {
        public long Id { get; set; }
        public long HotelId { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }

        public bool isActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        public long ModifiedBy { get; set; }
        public long DeletedBy { get; set; }
        public bool isDeleted { get; set; }

        public string message { get; set; }
        public int FlagId { get; set; }
        public IEnumerable<JobCategoriesDTO> hotelslist { get; set; }
        public IEnumerable<JobCategoriesDTO> jobcategorieslist { get; set; }
        public string datasetxml { get; set; }
        public string hotelname { get; set; }

    }
}