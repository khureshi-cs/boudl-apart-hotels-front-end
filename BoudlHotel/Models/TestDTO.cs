﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoudlHotel
{
    public class TestDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string message { get; set; }
        public IEnumerable<TestDTO> Testlist { get; set; }
        public int FlagId { get; set; }
    }
}